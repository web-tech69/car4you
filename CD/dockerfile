# Build stage
#
FROM maven:3.9.1-eclipse-temurin-17 AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

# Use latest jboss/base-jdk:11 image as the base
FROM openjdk:17-jdk-bullseye

# Set the WILDFLY_VERSION env variable
ENV WILDFLY_VERSION 26.1.3.Final
ENV JBOSS_HOME /opt/jboss/wildfly

USER root
WORKDIR /app
# Add the WildFly distribution to /opt, and make wildfly the owner of the extracted tar content
# Make sure the distribution is available from a well-known place
RUN wget -q https://github.com/wildfly/wildfly/releases/download/26.1.3.Final/wildfly-preview-$WILDFLY_VERSION.tar.gz \
    && tar -xf wildfly-preview-$WILDFLY_VERSION.tar.gz \
    && mkdir -p $JBOSS_HOME \
    && mv wildfly-preview-$WILDFLY_VERSION/* $JBOSS_HOME \
    && rm wildfly-preview-$WILDFLY_VERSION.tar.gz

# Ensure signals are forwarded to the JVM process correctly for graceful shutdown
ENV LAUNCH_JBOSS_IN_BACKGROUND true

# Add the WildFly admin user

WORKDIR $JBOSS_HOME/bin
RUN ./add-user.sh -u 'admin' -p 'admin'

RUN mkdir -p $JBOSS_HOME/modules/system/layers/base/com/mysql/main

#COPY CD/mysql-connector-j-8.0.32.jar $JBOSS_HOME/modules/system/layers/base/com/mysql/main/
COPY CD/mysql-connector-j-8.0.32.jar /home/
#COPY CD/module.xml $JBOSS_HOME/modules/system/layers/base/com/mysql/main/module.xml

COPY --from=build /home/app/target/cars4you-1.0-SNAPSHOT.war /opt/jboss/wildfly/standalone/deployments/

RUN mkdir /opt/jboss/wildfly/standalone/log

#Copy Database script to container
COPY CD/dbscript.cli /home/

# Expose the ports in which we're interested
EXPOSE 8080

# Set the default command to run on boot
# This will boot WildFly in standalone mode and bind to all interfaces
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]

#RUN docker exec wildfly bash -c "./jboss-cli.sh --file=/home/dbscript.cli"