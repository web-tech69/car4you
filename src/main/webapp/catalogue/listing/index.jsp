<%--Author: Andreas Ott--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
    <link rel="stylesheet" type="text/css" href="../../styles/global.css">
    <link rel="stylesheet" type="text/css" href="../../styles/header.css">
    <link rel="stylesheet" type="text/css" href="../../styles/models.css">
    <link rel="stylesheet" type="text/css" href="../../styles/singleModel.css">
    <script src="../../utils/catalogue-listing.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
    <title>Model — Cars4You</title>
</head>
<body id="app">
<jsp:include page="../../includes/header.jsp"/>
<jsp:include page="../../SingleListingServlet"/>
<div class="single-model-wrapper">
    <div>
        <a class="back-button-a" href="/cars4you/catalogue">⇐ Zurück</a>
    </div>
        <h3 class="single-listing-heading">Modell: ${currentListing.model.manufacturer} ${currentListing.model.modelName}</h3>
    <div class="horizontal-alignment">
        <button class="next-image-button" id="previous-img"><img alt="vorheriges Foto" class="next-image-button-img" src="../../includes/previousImage.png"></button>
            <div class="single-listing-image-div">
                <c:forEach items="${currentListing.imageUuid}" var="image" varStatus="listingNumber">
                    <img class="single-model-image" src="/cars4you/ImageServlet?uuid=${image}" alt="Abbidlung des Autos">
                </c:forEach>
            </div>
        <button class="next-image-button" id="next-img"><img alt="nächstes Foto" class="next-image-button-img" src="../../includes/nextImage.png"></button>
    </div>
        <input value="${currentListing.uuid}" type="hidden">
        <a class="button single-listing-booking-button" href="/cars4you/booking?currentListing=${currentListing.uuid}">Buchen</a>
    <div class="car-stats-group">
        <h3>Allgemeine Informationen</h3>
        <div class="car-stats">
            <p>Preis</p>
            <p>${currentListing.price}€</p>
        </div>
        <div class="car-stats">
            <p>Beschreibung</p>
            <p>${currentListing.description}</p>
        </div>
        <div class="car-stats">
            <p>Eingestellt von</p>
            <p>${currentListing.author.lastname}</p>
        </div>
        <div class="car-stats">
            <p>Eingestellt am</p>
            <p>${currentListing.createdDate}</p>
        </div>
    </div>
    <div class="car-stats-group">
        <h3>Technische Daten</h3>
        <div class="car-stats">
            <p>Antriebsart</p>
            <p>${currentListing.fuelType}</p>
        </div>
        <div class="car-stats">
            <p>Leistung</p>
            <p>${currentListing.horsepower}ps</p>
        </div>
        <div class="car-stats">
            <p>Farbe</p>
            <p>${currentListing.color}</p>
        </div>
        <div class="car-stats">
            <p>Anzahl Türen</p>
            <p>${currentListing.doors}</p>
        </div>
        <div class="car-stats">
            <p>Anzahl Sitze</p>
            <p>${currentListing.seats}</p>
        </div>
        <div class="car-stats">
            <p>Baujahr</p>
            <p>${currentListing.manufacturingYear}</p>
        </div>
    </div>
</div>
<jsp:include page="../../includes/footer.jsp"/>
</body>
</html>