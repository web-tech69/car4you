<%--suppress LanguageDetectionInspection --%>
<%--Author: Andreas Ott--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
    <link rel="stylesheet" type="text/css" href="../styles/global.css">
    <link rel="stylesheet" type="text/css" href="../styles/header.css">
    <link rel="stylesheet" type="text/css" href="../styles/models.css">
    <script src="../utils/get-according-models.js"></script>
    <script src="../utils/catalogue.js"></script>
    <script src="../utils/datepicker.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
    <title>Modelle — Cars4You</title>
</head>
<body id="app">
<jsp:include page="/ListingServlet" />
<jsp:include page="../includes/header.jsp"/>
    <main class="page catalogue">
        <aside class="sidebar">
            <form action="/cars4you/ResetSearchServlet">
                <button class="reset-search-button <c:if test="${search == null}">display-none</c:if>" >Filter zurücksetzen</button>
            </form>
            <form action="/cars4you/SearchServlet" method="get">
                <label for="brand-select">Marke</label>
                <select name="brand" id="brand-select">
                    <c:forEach items="${brands}" var="brand">
                        <option value="${brand}" <c:if test="${search.brand == brand}">selected</c:if> >${brand}</option>
                    </c:forEach>
                </select>
                <label for="model-select">Modell</label>
                <p class="display-none" id="get-model-to-javascript">${search.model}</p>
                <select name="model" id="model-select">
                    <option selected>
                        <c:if test="${search.model == '%' or search.model == null}">Alle</c:if>
                        <c:if test="${search.model != '%'}">${search.model}</c:if>
                    </option>
                </select>
                <label for="horsepower-select-min">Leistung von</label>
                <input type="number" id="horsepower-select-min" name="horsepower-min" placeholder="Mindestleistung" value="<c:if test="${search.minHorsepower != 0}">${search.minHorsepower}</c:if>">
                <label for="horsepower-select-max">Leistung bis</label>
                <input type="number" id="horsepower-select-max" name="horsepower-max" placeholder="Maximalleistung" value="<c:if test="${search.maxHorsepower != 9999}">${search.maxHorsepower}</c:if>">
                <label for="manufacturing-year-select-min">Baujahr von</label>
                <input type="number" id="manufacturing-year-select-min" name="manufacturing-year-min" placeholder="Mindestbaujahr" value="<c:if test="${search.minManufactureYear != 0}">${search.minManufactureYear}</c:if>">
                <label for="manufacturing-year-select-max">Baujahr bis</label>
                <input type="number" id="manufacturing-year-select-max" name="manufacturing-year-max" placeholder="Maximalbaujahr" value="<c:if test="${search.maxManufactureYear != 9999}">${search.maxManufactureYear}</c:if>">
                <label for="booked-from">Buchen von</label>
                <input id="booked-from" class="date-input" name="booked-from" type="date" required value="${search.bookedFrom}">
                <label for="booked-to">Buchen bis</label>
                <input id="booked-to" class="date-input" name="booked-to" type="date" required value="${search.bookedTo}">
                <label>Farbe</label>
                <div id="color-selector" class="colors">
                    <div class="black">
                        <input type="radio" name="color" id="color-black" value="Schwarz" class="display-none">
                    </div>
                    <div class="grey">
                        <input type="radio" name="color" id="color-grey" value="Grau" class="display-none">
                    </div>
                    <div class="white">
                        <input type="radio" name="color" id="color-white" value="Weiß" class="display-none">
                    </div>
                    <div class="red">
                        <input type="radio" name="color" id="color-red" value="Rot" class="display-none">
                    </div>
                    <div class="orange">
                        <input type="radio" name="color" id="color-orange" value="Orange" class="display-none">
                    </div>
                    <div class="yellow">
                        <input type="radio" name="color" id="color-yellow" value="Gelb" class="display-none">
                    </div>
                    <div class="green">
                        <input type="radio" name="color" id="color-green" value="Grün" class="display-none">
                    </div>
                    <div class="brown">
                        <input type="radio" name="color" id="color-brown" value="Braun" class="display-none">
                    </div>
                    <div class="pink">
                        <input type="radio" name="color" id="color-pink" value="Pink" class="display-none">
                    </div>
                    <div class="blue">
                        <input type="radio" name="color" id="color-blue" value="Blau" class="display-none">
                    </div>
                </div>
                <button type="submit" class="start-search-button">Suchen</button>
            </form>
        </aside>
        <div class="models-wrapper">
            <c:forEach items="${listings}" var="listing" varStatus="listingNumber">
                <c:set var="listingData" value="${listing}" scope="request"/>
                <jsp:include page="../includes/listing.jsp"></jsp:include>
            </c:forEach>
        </div>
        <jsp:include page="../includes/date-picker-popup.jsp"/>
    </main>
<jsp:include page="../includes/footer.jsp"/>
</body>
</html>