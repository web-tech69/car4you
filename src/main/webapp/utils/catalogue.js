// Author: Andreas Ott

// Create event listener for color divs
document.addEventListener("DOMContentLoaded", () => {
    let colorDivs = document.getElementById("color-selector").children;
    for(let i = 0; i < colorDivs.length; i++){
        colorDivs[i].addEventListener("click", highlightRadioBox);
    }
});

function highlightRadioBox(event) {
    let radioBoxes = document.getElementById("color-selector").children;
    for(let i = 0; i < radioBoxes.length; i++){
        radioBoxes[i].classList.remove("highlighted");
    }
    event.target.classList.add("highlighted");
    event.target.children[0].checked = true;
}