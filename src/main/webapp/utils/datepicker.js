// Author: Andreas Dinauer
let monthField;
let yearField;
let date;
let popup;
let target;

document.addEventListener("DOMContentLoaded", () => {
    init();
})

function init() {
    popup = document.getElementById("popup-overlay");
    monthField = document.getElementById("cur-month");
    yearField = document.getElementById("cur-year");

    let nextMonthButton = document.getElementById("next-month");
    let prevMonthButton = document.getElementById("prev-month");
    let closePopupButton = document.getElementById("close-popup");

    let dateInputs =  document.getElementsByClassName("date-input");
    for(let i = 0; i < dateInputs.length; i++) {
        dateInputs[i].addEventListener("click", (event) => {
            event.preventDefault();
            openPopup(event);
        })
    }

    closePopupButton.addEventListener("click", () => {
        closePopup();
    })

    //Vorherigen Monat Laden und Tage neu rendern
    prevMonthButton.addEventListener("click", () => {
        date.setMonth(date.getMonth() - 1);
        rerender();
    });
    //Nächsten Monat Laden und Tage neu rendern
    nextMonthButton.addEventListener("click", () => {
        date.setMonth(date.getMonth() + 1);
        rerender();
    });
}

function rerender() {
    let month = date.getMonth()
    let year = date.getFullYear();

    let months = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"];
    monthField.innerText = months[month];
    yearField.innerText = year;

    // Entfernt alle bereits vorhandenen Children des Containers.
    let dayContainer = document.getElementById("date-container");
    dayContainer.replaceChildren(); 

    let dayNames = ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"];
    for(let i = 0; i < dayNames.length; i++) {
        let dayElement = document.createElement("p");
        dayElement.innerText = dayNames[i];
        dayElement.classList.add("day-name");
        dayContainer.appendChild(dayElement);
    }

    let datePickerOffset = new Date(date.getFullYear(), date.getMonth(), 1).getDay(); //Abstand des ersten Tages der Woche zum Montag
    for(let i = 0; i < datePickerOffset; i++) {
        let dayElement = document.createElement("p");
        dayElement.innerText = i.toString();
        dayElement.classList.add("day");
        dayElement.style.visibility = "hidden";
        dayContainer.appendChild(dayElement);
    }

    let daysInMonth = getDaysInMonth(year, month);
    for(let i = 0; i < daysInMonth; i++) {
        let day = i + 1;
        let loopDate = new Date(date.getFullYear(), date.getMonth(), day);
        let dayElement = document.createElement("p");
        dayElement.innerText = day.toString();
        dayElement.classList.add("day");
        dayContainer.appendChild(dayElement);

        //Tage die in der Vergangenheit liegen werden durchgestrichen.
        let futureDays = loopDate > new Date().setDate(new Date().getDate() - 1);
        if(futureDays) {
            dayElement.classList.add("present");
            dayElement.addEventListener("click", () => {
                writeDateToInput(loopDate);
            });
        }
    }

    let placeholders = 7*6 - (datePickerOffset + daysInMonth); //Platzhalter am Ende des Datepickers einfügen, um gleiche Höhe des Popups zu gewährleisten
    for(let i = 0; i < placeholders; i++) {
        let dayElement = document.createElement("p");
        dayElement.innerText = i.toString();
        dayElement.classList.add("day");
        dayElement.style.visibility = "hidden";
        dayContainer.appendChild(dayElement);
    }
}

// Schreibt das Datum in den zugehörigen Input
function writeDateToInput(date) {
    target.value = formatDate(date);
    target.dispatchEvent(new Event('change'));
    closePopup();
}

function formatDate(date) {
    let month = date.getMonth() + 1;
    if(month < 10) {
        month = '0' + month;
    }
    let day = date.getDate();
    if(day < 10) {
        day = '0' + day;
    }
    return date.getFullYear() + "-" + month + "-" + day;
}

/* Quelle https://stackoverflow.com/questions/1184334/get-number-days-in-a-specified-month-using-javascript */
function getDaysInMonth(year, month) { 
    //Parameter "month" beginnt mit Index 0!
    return new Date(year, month + 1, 0).getDate();
}

function openPopup(event) {
    popup.style.display = "flex";
    date = new Date();
    // Setzt HTML-Input in welchen des Datum bei Auswahl geschrieben wird.
    target = event.target;
    rerender();
}

function closePopup() {
    popup.style.display = "none";
}