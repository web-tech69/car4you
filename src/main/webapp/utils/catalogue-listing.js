// Author: Andreas Ott, Andreas Dinauer
document.addEventListener("DOMContentLoaded", () => {
    let index = 0;
    displayImage();
    document.getElementById("next-img").addEventListener("click", () => {
        displayImage(++index);
    })
    document.getElementById("previous-img").addEventListener("click", () => {
        displayImage(--index);
    })
});

function displayImage(index) {

    const images = document.getElementsByClassName("single-model-image");
    for (const image of images) {
        image.style.display = "none";
    }
    /*
    1 % 5 = 1
    2 % 5 = 2
    3 % 5 = 3
    4 % 5 = 4
    5 % 5 = 0
    6 % 5 = 1
    7 % 5 = 2
    -1 % 5 = 4
    -2 % 5 = 3
    -3 % 5 = 2
    -4 % 5 = 1
    -5 % 5 = 0
    -6 % 5 = 4
    -7 % 5 = 3
    */
    images.item(Math.abs(index%images.length)).style.display = "block";
}