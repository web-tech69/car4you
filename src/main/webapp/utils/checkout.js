// Author: Andreas Dinauer

let totalPrice;
let warningBox;
document.addEventListener("DOMContentLoaded", () => {
    let bookedFrom = document.getElementById("booked-from");
    let bookedTo = document.getElementById("booked-to");
    totalPrice = document.getElementById("total");
    warningBox = document.getElementById("warning-box");

    bookedFrom.addEventListener("change", updateTotal);
    bookedTo.addEventListener("change", updateTotal);
    updateTotal();
});

function updateTotal() {
    let bookedFrom = document.getElementById("booked-from");
    let bookedTo = document.getElementById("booked-to");
    hideWarning();
    hideTotal();

    if(bookedFrom.value && bookedTo.value) {
        let dateFrom = new Date(bookedFrom.value);
        let dateTo = new Date(bookedTo.value);
        let yesterday = new Date().setDate(new Date().getDate() - 1);
        if(dateFrom > yesterday) {
            if(dateFrom < dateTo) {
                setTotal(dateFrom, dateTo);
                return;
            } else {
                showWarning("Enddatum liegt vor Startdatum der Buchung!");
            }
        } else {
            showWarning("Datum liegt in der Vergangenheit!");
        }
    }
}

function showWarning(text) {
    warningBox.style.display = "flex";
    warningBox.innerText = text;
    document.getElementById("booking-button").disabled = true;
}

function hideWarning() {
    warningBox.style.display = "none";
    document.getElementById("booking-button").disabled = false;
}

function setTotal(dateFrom, dateTo) {
    let timeDif = dateTo.getTime() - dateFrom.getTime();
    let days = timeDif/(1000*60*60*24);
    let pricePerDay = document.getElementById("daily-price").innerText;
    showTotal(days*pricePerDay);
}

function showTotal(text) {
    document.getElementById("total-value").innerText = text;
    totalPrice.style.display = "flex";
}

function hideTotal() {
    totalPrice.style.display = "none";
}