// Author: Andreas Ott
document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("brand-select").addEventListener("change", (event) => {
        getAccordingModels(event.target.value, document.getElementById("model-select"));
    });
});
function getAccordingModels(brand, targetElement) {
    let request = new XMLHttpRequest();
    request.responseType = "json";
    request.open("GET", "/cars4you/GetModelsServlet?brand=" + brand, true);
    request.onreadystatechange = function () {
        if (request.readyState === 4 && request.status === 200) {
            // receive json array of model names
            let modelNames = request.response;
            targetElement.replaceChildren();
            addAllModelsOption(targetElement);
            addOptions(modelNames, targetElement);
        }
    }
    request.send();
}

function addOptions(modelNames, targetElement) {

    let selected = document.getElementById("get-model-to-javascript");
    if(selected != null){
        selected = selected.innerHTML;
    } else {
        selected = "";
    }
    modelNames.forEach(function (model_name) {
        let option = document.createElement("option");
        option.setAttribute("value", model_name.trim());
        option.innerHTML = model_name.trim();

        if(selected != null && model_name.trim() === selected.trim()){
            option.setAttribute("selected", "selected")
        }
        targetElement.appendChild(option);
    });
}

function addAllModelsOption(targetElement) {
    let option = document.createElement("option");
    option.setAttribute("value", "");
    option.innerHTML = "Alle";
    targetElement.appendChild(option);
}

window.onload = function () {
    let brand = document.getElementById("brand-select").value;
    let targetElement = document.getElementById("model-select");
    getAccordingModels(brand, targetElement);
}