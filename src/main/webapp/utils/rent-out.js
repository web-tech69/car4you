// Author: Johannes Buchberger
const reader = new FileReader();
function showPreview(event) {
    if(event.target.files.length > 0){
        for(let i = 0; i < event.target.files.length; i++){
            reader.readAsDataURL(event.target.files[i]);
            reader.onload = function(){
                let previewDiv = document.getElementById("image-preview");
                let preview = document.createElement("img");
                let previewImageInput = document.createElement("input");
                let y = 0;
                while(document.getElementsByName("image" + y).length > 0){
                    y++;
                }
                previewImageInput.setAttribute("type", "hidden");
                previewImageInput.setAttribute("name", "image" + y);
                previewImageInput.setAttribute("value", reader.result.substring(reader.result.indexOf(",") + 1));
                document.getElementById("image").setAttribute("value", "");
                previewDiv.appendChild(previewImageInput);
                preview.src = reader.result;
                previewDiv.appendChild(preview);
            }
        }
    }
}

function highlightRadioBox(event) {
    let radioBoxes = document.getElementsByClassName("radio-input");
    for(let i = 0; i < radioBoxes.length; i++){
        radioBoxes[i].classList.remove("highlighted");
    }
    event.target.parentElement.classList.add("highlighted");
}