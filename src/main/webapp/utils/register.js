// Author: Johannes Buchberger
function sendFormAfterPasswordCheck(event) {
    let loginButton = event.target;
    let loginForm = loginButton.closest("form");
    const formData = new FormData(loginForm);

    if (formData.get("password") !== formData.get("password-repeat")) {
        event.preventDefault();
        setWarning("Passwörter stimmen nicht überein");
        // um zu zeigen das wir wissen wie man alerts macht kann man alternativ auch das hier machen
        // alert("Passwörter stimmen nicht überein!");
        return;
    }
}

// Author: Johannes Buchberger
document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("register-form").addEventListener("submit", (event) => {
        sendFormAfterPasswordCheck(event);
    });
});

function setWarning(text) {
    let warningBox = document.getElementById("register-warning-box");
    warningBox.innerText = text;
    warningBox.style.display = "flex";
}