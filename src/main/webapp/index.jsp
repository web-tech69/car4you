<%--Author: Andreas Johannes Dinauer--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
    <head>
        <link rel="stylesheet" type="text/css" href="styles/global.css">
        <link rel="stylesheet" type="text/css" href="styles/header.css">
        <link rel="stylesheet" type="text/css" href="styles/models.css">

        <script src="utils/get-according-models.js"></script>
        <script src="utils/datepicker.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
        <title>Cars4You</title>
    </head>
    <body id="app">
        <jsp:include page="/SearchComponentServlet"/>
        <jsp:include page="includes/header.jsp"/>
        <div class="page">
            <jsp:include page="includes/search-component.jsp"/>
            <jsp:include page="/includes/recommended.jsp"/>
        </div>
        <jsp:include page="includes/footer.jsp"/>
        <jsp:include page="includes/date-picker-popup.jsp"/>
    </body>
</html>