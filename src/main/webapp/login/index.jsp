<%-- Author: Johannes Buchberger--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="de">
    <head>
        <link rel="stylesheet" type="text/css" href="../styles/global.css">
        <link rel="stylesheet" type="text/css" href="../styles/login.css">
        <link rel="stylesheet" type="text/css" href="../styles/header.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
        <script src="../utils/register.js"></script>
        <title>Login/Register — Cars4You</title>
    </head>
    <body id="app">
        <jsp:include page="../includes/header.jsp"/>
        <main class="page">
            <div class="col-2">
                <form action="/cars4you/LoginServlet" method="post">
                    <c:if test="${loginFail}">
                        <div class="error-box">
                            <p class="error-label">Ungültiger Login</p>
                        </div>
                    </c:if>
                    <div class="warning-box" id="login-warning-box"></div>
                    <label>E-Mail</label>
                    <input type="email" name="email" maxlength="255" required>
                    <label>Passwort</label>
                    <input type="password" name="password" maxlength="255" required>
                    <button class="center-button login-button" id="login-button" type="submit">Login</button>
                </form>
                <form action="/cars4you/RegisterServlet" id="register-form" method="post">
                    <c:if test="${registerFail}">
                        <div class="error-box">
                            <p class="error-label">Ungültige Registrierung</p>
                        </div>
                    </c:if>
                    <div class="warning-box" id="register-warning-box"></div>
                    <label>Vorname</label>
                    <input type="text" name="firstname" maxlength="255" required>
                    <label>Nachname</label>
                    <input type="text" name="lastname" maxlength="255" required>
                    <label>E−Mail</label>
                    <input type="email" name="email" maxlength="255" required>
                    <label>Passwort</label>
                    <input type="password" name="password" minlength="6" maxlength="255" required>
                    <label>Passwort Wiederholen</label>
                    <input type="password" name="password-repeat" minlength="6" maxlength="255" required>
                    <button class="center-button login-button" type="submit" id="register-button">Registrieren</button>
                </form>
            </div>
        </main>
    <jsp:include page="../includes/footer.jsp"/>
    </body>
</html>
