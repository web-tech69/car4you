<%-- Author: Johannes Buchberger, Andreas Dinauer --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="booking" type="de.in.cars4you.jpa.entity.Booking"--%>
<!DOCTYPE html>
<html lang="de">
<head>
    <link rel="stylesheet" type="text/css" href="../../styles/global.css">
    <link rel="stylesheet" type="text/css" href="../../styles/header.css">
    <link rel="stylesheet" type="text/css" href="../../styles/booking.css">
    <link rel="stylesheet" type="text/css" href="../../styles/account/account.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
    <title>Buchungen — Cars4You</title>
</head>
<body id="app">
<jsp:include page="../../BookingsServlet"/>
    <jsp:include page="../../includes/header.jsp"/>
    <main class="page">
        <jsp:include page="../includes/header.jsp"/>
        <div class="account-container">
            <jsp:include page="../includes/sidebar.jsp"/>
            <div class="account-content">
                <c:if test="${param.success != null}">
                    <p class="space success-container"><span class="material-symbols-outlined">check_circle</span> Buchung erfolgreich</p>
                </c:if>

                <h1 class="space">Buchungen</h1>
                    <c:if test="${bookings == null}">
                        <p>Keine Buchungen vorhanden</p>
                    </c:if>
                <div class="bookings-container">
                    <c:forEach items="${bookings}" var="booking">
                        <c:set var="bookingData" value="${booking}" scope="request"/>
                        <jsp:include page="../../includes/booking.jsp"></jsp:include>
                    </c:forEach>
                </div>
            </div>
        </div>
    </main>
    <jsp:include page="../../includes/footer.jsp"/>
</body>
</html>
