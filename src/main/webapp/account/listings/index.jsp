<%-- Author: Johannes Buchberger, Andreas Ott --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="de">
<head>
  <link rel="stylesheet" type="text/css" href="../../styles/global.css">
  <link rel="stylesheet" type="text/css" href="../../styles/header.css">
  <link rel="stylesheet" type="text/css" href="../../styles/account/account.css">
  <link rel="stylesheet" type="text/css" href="../../styles/account/listings.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
  <title>Inserate — Cars4You</title>
</head>
<body id="app">
<jsp:include page="../../InserateServlet"></jsp:include>
<jsp:include page="../../includes/header.jsp"/>
<main class="page">
  <jsp:include page="../includes/header.jsp"/>
  <div class="account-container">
    <jsp:include page="../includes/sidebar.jsp"/>
    <div class="account-content">
      <c:if test="${param.success != null}">
        <p class="space success-container"><span class="material-symbols-outlined">check_circle</span> Inserat erstellt!</p>
      </c:if>
      <h1 class="space">Inserate</h1>
      <div class="listing-container">
        <c:if test="${listings == null}">
          <p>Keine Inserate erstellt</p>
        </c:if>
      </div>
      <section id="listing-container">
        <h3>Bild</h3>
        <h3>Hersteller</h3>
        <h3>Modell</h3>
        <h3>Baujahr</h3>
        <h3>Farbe</h3>
        <h3>Preis</h3>
        <p class="placeholder"></p>  <!--Wird benötigt um 6 Spalten zu gewährleisten (Platzhalter)-->
        <c:forEach items="${listings}" var="listing">
          <c:set var="listingData" value="${listing}" scope="request"/>
          <jsp:include page="../includes/account-listing.jsp"></jsp:include>
        </c:forEach>
      </section>
    </div>
  </div>
</main>
<jsp:include page="../../includes/footer.jsp"/>
</body>
</html>
