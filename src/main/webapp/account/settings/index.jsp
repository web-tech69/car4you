<%--Author: Johannes Buchberger--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="de">
<head>
    <link rel="stylesheet" type="text/css" href="../../styles/global.css">
    <link rel="stylesheet" type="text/css" href="../../styles/header.css">
    <link rel="stylesheet" type="text/css" href="../../styles/account/account.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
    <title>Einstellungen — Cars4You</title>
</head>
<body id="app">
<jsp:include page="../../includes/header.jsp"/>
<jsp:include page="../../SettingsServlet"/>
<main class="page">
    <jsp:include page="../includes/header.jsp"/>
    <div class="page account-container">
        <jsp:include page="../includes/sidebar.jsp"/>
        <div class="account-content">
            <h2 class="space">Einstellungen</h2>
            <form action="/cars4you/SettingsServlet" method="post">
                <label for="firstname">Neuer Vorname</label>
                <input id="firstname" type="text" name="firstname" maxlength="255" value="${user.firstname}">
                <label for="lastname">Neuer Nachname</label>
                <input id="lastname" type="text" name="lastname" maxlength="255" value="${user.lastname}">
                <label for="email">Neue E−Mail</label>
                <input id="email" type="email" name="email" maxlength="255" value="${user.email}">
                <label for="password">Neues Passwort</label>
                <input autocomplete="new-password" id="password" type="password" name="password" minlength="6" maxlength="255">
                <button class="nav-item call-to-action button" type="submit">Speichern</button>
            </form>
            <c:if test="${updateSuccess == true}">
                <p>data successfully updated</p>
            </c:if>
        </div>
    </div>
</main>
<jsp:include page="../../includes/footer.jsp"/>
</body>
</html>
