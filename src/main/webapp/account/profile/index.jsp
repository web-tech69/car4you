<%--Author: Johannes Buchberger--%>
<%--@elvariable id="user" type="de.in.cars4you.jpa.entity.User"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="de">
<head>
  <link rel="stylesheet" type="text/css" href="../../styles/global.css">
  <link rel="stylesheet" type="text/css" href="../../styles/header.css">
  <link rel="stylesheet" type="text/css" href="../../styles/account/account.css">
  <link rel="stylesheet" type="text/css" href="../../styles/account/profile.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
  <title>Profil — Cars4You</title>
</head>
<body id="app">
<jsp:include page="../../includes/header.jsp"/>
<main class="page">
  <jsp:include page="../includes/header.jsp"/>
  <div class="page account-container">
    <jsp:include page="../includes/sidebar.jsp"/>
    <div class="account-content">
      <h2 class="space">Profile</h2>
      <div class="profile-info">
        <p>Vorname</p>
        <p>${user.firstname}</p>
      </div>
      <div class="profile-info">
        <p>Nachname</p>
        <p>${user.lastname}</p>
      </div>
      <div class="profile-info">
        <p>Email</p>
        <p>${user.email}</p>
      </div>
    </div>
  </div>
</main>
<jsp:include page="../../includes/footer.jsp"/>
</body>
</html>
