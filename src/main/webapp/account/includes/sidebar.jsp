<%--Author: Johannes Buchberger--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<aside class="account-nav">
    <a class="nav-item <c:if test="${accountMenuSelection == 1}">nav-selected</c:if>" href="/cars4you/ProfileServlet">Profil</a>
    <a class="nav-item <c:if test="${accountMenuSelection == 2}">nav-selected</c:if>" href="/cars4you/account/bookings">Buchungen</a>
    <a class="nav-item <c:if test="${accountMenuSelection == 3}">nav-selected</c:if>" href="/cars4you/account/listings">Inserate</a>
    <a class="nav-item <c:if test="${accountMenuSelection == 4}">nav-selected</c:if>" href="/cars4you/account/settings">Einstellungen</a>
    <a class="nav-item call-to-action button" href="/cars4you/LogoutServlet">Ausloggen</a>
</aside>