<%-- Author: Andreas Dinauer --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header class="account-header">
    <p class="main-heading">Hallo, ${user.firstname}</p>
    <p class="sub-heading">Hier kannst du deine Profileinstellungen anpassen</p>
</header>