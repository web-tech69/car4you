<%--Author: Johannes Buchberger--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<img class="left" src="/cars4you/ImageServlet?uuid=${listingData.imageUuid[0]}" alt="">
<p class="center">${listingData.model.manufacturer}</p>
<p class="center">${listingData.model.modelName}</p>
<p class="center">${listingData.manufacturingYear}</p>
<p class="center">${listingData.color}</p>
<p class="center">${listingData.price} €</p>
<form class="right" action="/cars4you/InserateServlet" method="post">
  <input type="hidden" name="listingId" value="${listingData.uuid}">
  <button class="delete-button" type="submit">
    <span class="material-symbols-outlined">delete</span>
  </button>
</form>