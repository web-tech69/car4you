<%--Author: Andreas Ott--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
    <title>Admin — Cars4You</title>
    <link rel="stylesheet" type="text/css" href="../styles/global.css">
    <link rel="stylesheet" type="text/css" href="../styles/header.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
    <link rel="stylesheet" type="text/css" href="../styles/models.css">
    <link rel="stylesheet" type="text/css" href="../styles/admin.css">
    <script src="../utils/admin.js"></script>
</head>
<body id="app">
    <jsp:include page="/AdminDataServlet"/>
    <jsp:include page="../includes/header.jsp"/>
    <main class="two-col-grid">
        <div>
            <h2>Benutzerverwaltung</h2>
            <div class="user-verwaltung">
                <c:forEach items="${users}" var="user" varStatus="userNumber">
                    <div class="user-row">
                        <p>${user.lastname}, ${user.firstname}</p>
                        <p>${user.email}</p>
                        <div class="actions-div">
                            <button class="admin-action-button" onclick="showPasswordField('${user.uuid}')">
                                <span class="material-symbols-outlined">lock_reset</span>
                            </button>
                            <form action="/cars4you/DeleteUserServlet" method="post">
                                <input type="hidden" name="userId" value="${user.uuid}">
                                <button class="admin-action-button" type="submit">
                                    <span class="material-symbols-outlined">delete</span>
                                </button>
                            </form>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <div class="new-password-div" id="new-password-div">
                <h3>Neues Passwort setzen</h3>
                <form action="/cars4you/ResetPasswordServlet" method="post">
                    <input type="hidden" name="userId" id="user-to-change" value="">
                    <input type="password" name="newPassword" id="newPassword" placeholder="Neues Passwort">
                    <button class="booking-button" type="submit">Passwort zurücksetzen</button>
                </form>
            </div>
        </div>
        <div>
            <h2>Anzeigenverwaltung</h2>
            <div class="listings-verwaltung">
                <c:forEach items="${listings}" var="listing" varStatus="listingNumber">
                        <div class="model">
                            <img class="model-image" alt="listing-${listing.model.manufacturer}-${listing.model.modelName}" src="/cars4you/ImageServlet?uuid=${listing.imageUuid[0]}">
                            <div class="model-heading-div">
                                <h3 class="model-heading"><c:out value="${listing.model.manufacturer}"/></h3>
                                <h3 class="model-heading"><c:out value="${listing.model.modelName}"/></h3>
                            </div>
                            <div class="car-stats-admin">
                                <p>Leistung</p>
                                <p><c:out value="${listing.horsepower}"/>ps</p>
                            </div>
                            <div class="car-stats-admin">
                                <p>Baujahr</p>
                                <p><c:out value="${listing.manufacturingYear}"/></p>
                            </div>
                            <div class="car-stats-admin">
                                <p>Preis</p>
                                <p><c:out value="${listing.price}"/>€</p>
                            </div>
                            <div class="actions-div">
                                <form action="/cars4you/DeleteListingServlet" method="post">
                                    <input type="hidden" name="listingId" value="${listing.uuid}">
                                    <button class="admin-action-button booking-button" type="submit">
                                        Löschen
                                    </button>
                                </form>
                            </div>
                        </div>
                </c:forEach>
            </div>
        </div>

    </main>
    <jsp:include page="../includes/footer.jsp"/>
</body>
</html>