<%--Author: Andreas Dinauer--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="/RecommendedListingServlet"/>
<section>
    <h2 class="space heading">Empfohlen</h2>
    <c:forEach items="${recommended}" var="recommendedListing">
        <h3 class="space">${recommendedListing.brandName}</h3>
        <div class="col-4 space">
            <c:forEach items="${recommendedListing.listings}" var="listing">
                <c:set var="listingData" value="${listing}" scope="request"/>
                <jsp:include page="listing.jsp"></jsp:include>
            </c:forEach>
        </div>
    </c:forEach>
</section>