<%--Author: Andreas Dinauer--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="popup-overlay">
  <div class="popup-content">
    <header class="date-picker-header">
      <h2>Datum Wählen</h2>
      <p id="close-popup">&times;</p>
    </header>
    <div id="month-select">
      <p id="prev-month"><span class="material-symbols-outlined">chevron_left</span></p>
      <p><span id="cur-month">Month</span> <span id="cur-year">Year</span></p>
      <p id="next-month"><span class="material-symbols-outlined">chevron_right</span></p>
    </div>
    <div id="date-container"></div>
  </div>
</div>