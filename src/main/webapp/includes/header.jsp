<%--Author: Johannes Buchberger--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<noscript><p>JavaScript ist deaktviert. Bitte aktivieren sie JavaScript</p></noscript>
<header class="header">
    <a class="site-ident" href="/cars4you/">
        <img alt="Site Logo (Blue Car)" class="site-logo" src="/cars4you/includes/logo.png">
        <h1>Cars<span class="colored-text">4</span>You</h1>
    </a>
    <nav class="header-nav">
        <a class="nav-link <c:if test="${globalMenuSelection == 2}">nav-selected</c:if>" href="/cars4you/catalogue">Katalog</a>
        <a class="nav-link <c:if test="${globalMenuSelection == 3}">nav-selected</c:if>" href="/cars4you/NewListingServlet">Vermieten</a>
        <a class="nav-link <c:if test="${globalMenuSelection == 1}">nav-selected</c:if> <c:if test="${user.admin != true}">display-none</c:if>" href="/cars4you/admin">Verwaltung</a>
    </nav>
    <nav>
        <a class="call-to-action button" id="login-btn" href="/cars4you/ProfileServlet">
            <c:if test="${user != null}">
                <span class="material-symbols-outlined">home</span>Account
            </c:if>
            <c:if test="${user == null}">
                <span class="material-symbols-outlined">login</span>
                <p>Login</p>
            </c:if>
        </a>
    </nav>
</header>
