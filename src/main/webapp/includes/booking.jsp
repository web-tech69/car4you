<%--Author: Andreas Dinauer--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<article class="booking">
  <img src="/cars4you/ImageServlet?uuid=${bookingData.listing.imageUuid[0]}" alt="">
  <div class="booking-info">
    <div>
      <h2 class="booking-heading">${bookingData.listing.model.manufacturer}, ${bookingData.listing.model.modelName}</h2>
      <p class="space">${bookingData.bookedFrom} - ${bookingData.bookedTo}, gebucht am ${bookingData.bookingDatetime}</p>
    </div>
    <div class="price-container">
      <p class="booking-price">${bookingData.days} Tage x ${bookingData.listing.price} €</p>
      <p class="booking-price">Gesamt: ${bookingData.priceTotal} €</p>
    </div>
  </div>
</article>
