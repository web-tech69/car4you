<%--Author: Andreas Johannes Dinauer--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<footer class="footer">
    <div class="inner-footer">
        <nav class="nav-footer">
            <a class="nav-link" href="link">Impressum</a>
            <a class="nav-link" href="link">Datenschutz</a>
            <a class="nav-link" href="link">AGB</a>
            <p class="copyright nav-link"> Cars4You</p>
        </nav>
    </div>
</footer>