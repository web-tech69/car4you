<%--Author: Andreas Ott--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<article class="model">
    <a class="model-anchor" href="/cars4you/catalogue/listing?currentListing=${listingData.uuid}">
        <div>
            <img class="model-image" alt="listing-<c:out value="${listingData.model.manufacturer}"/>-<c:out value="${listingData.model.modelName}"/>" src="/cars4you/ImageServlet?uuid=<c:out value="${listingData.imageUuid[0]}"/>">
            <div class="model-heading-div">
                <h3 class="model-heading"><c:out value="${listingData.model.manufacturer}"/></h3>
                <h3 class="model-heading"><c:out value="${listingData.model.modelName}"/></h3>
            </div>
        </div>
        <div>
            <div class="car-stats">
                <p>Leistung</p>
                <p><c:out value="${listingData.horsepower}"/>ps</p>
            </div>
            <div class="car-stats">
                <p>Baujahr</p>
                <p><c:out value="${listingData.manufacturingYear}"/></p>
            </div>
            <div class="car-stats">
                <p>Preis</p>
                <p><c:out value="${listingData.price}"/>€</p>
            </div>
            <p class="booking-button button">Buchen</p>
        </div>
    </a>
</article>