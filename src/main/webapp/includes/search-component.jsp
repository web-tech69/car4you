<%--Author: Andreas Dinauer--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="search-container">
  <div class="overlay">
    <form class="car-selector" action="/cars4you/SearchServlet" method="get">
      <div class="col-2">
        <div>
          <label>Marke</label>
          <select name="brand" id="brand-select">
            <c:forEach items="${brands}" var="brand">
              <option value="${brand}">${brand}</option>
            </c:forEach>
          </select>
        </div>
        <div>
          <label>Model</label>
          <select name="model" id="model-select">
            <option value="">Alle</option>
          </select>
        </div>
      </div>
      <div class="col-2">
        <div>
          <label for="booked-from">Von</label>
          <input id="booked-from" name="booked-from" type="date" class="date-input">
        </div>
        <div>
          <label for="booked-to">Bis</label>
          <input id="booked-to" name="booked-to" type="date" class="date-input">
        </div>
      </div>
      <button class="center-button">Suchen</button>
    </form>
  </div>
</div>