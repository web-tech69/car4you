<%--Author: Andreas Ott--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
    <link rel="stylesheet" type="text/css" href="../styles/global.css">
    <link rel="stylesheet" type="text/css" href="../styles/header.css">
    <link rel="stylesheet" type="text/css" href="../styles/rent-out.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
    <script src="../utils/get-according-models.js"></script>
    <script src="../utils/rent-out.js"></script>
    <title>Modelle — Cars4You</title>
</head>
<body id="app">
    <jsp:include page="../includes/header.jsp"/>
    <main class="rent-out-page">
            <form action="/cars4you/NewListingServlet" method="post">
                <div class="listing-input">
                    <div class="brand-model-wrapper">
                        <div>
                            <label for="brand-select">Marke</label>
                            <select name="brands" id="brand-select">
                                <optgroup label="Marke">
                                    <c:forEach items="${brands}" var="brand">
                                        <option value="${brand}">${brand}</option>
                                    </c:forEach>
                                </optgroup>
                            </select>
                        </div>
                        <div>
                            <label for="model-select">Modell</label>
                            <select name="model" id="model-select">
                                <optgroup label="model">
                                    <option disabled selected hidden>Modell</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <label for="year">Zulassungsjahr</label>
                    <input type="number" name="year" id="year" min="1500" max="9999" required>
                    <label for="fuelType">Kraftstoff</label>
                    <select name="fuelType" id="fuelType"  required>
                        <option value="" disabled selected hidden>Kraftstoff</option>
                        <option value="GASOLINE">Benzin</option>
                        <option value="DIESEL">Diesel</option>
                        <option value="ELECTRIC">Elektro</option>
                        <option value="HYBRID">Hybrid</option>
                        <option value="HYDROGEN">Wasserstoff</option>
                        <option value="OTHER">Andere</option>
                    </select>
                    <label for="fuelCapacity">Tankgröße</label>
                    <input type="number" name="fuelCapacity" id="fuelCapacity" min="0" max="9999" required>
                    <label for="horsepower">Leistung</label>
                    <input type="number" name="horsepower" id="horsepower" min="0" max="9999" required>
                    <label for="seats">Sitzplätze</label>
                    <input type="number" name="seats" id="seats" min="0" max="99" required>
                    <label>Türen</label>
                    <div class="radio-input-wrapper">
                        <div class="radio-input">
                            <label for="doors23">2/3</label>
                            <input type="radio" name="doors" id="doors23" value="2/3" onchange="highlightRadioBox(event)">
                        </div>
                        <div class="radio-input">
                            <label for="doors45">4/5</label>
                            <input type="radio" name="doors" id="doors45" value="4/5" onchange="highlightRadioBox(event)">
                        </div>
                        <div class="radio-input">
                            <label for="doors67">6/7</label>
                            <input type="radio" name="doors" id="doors67" value="6/7" onchange="highlightRadioBox(event)">
                        </div>
                    </div>
                    <label for="color">Farbe</label>
                    <input type="text" name="color" id="color" maxlength="255" required>
                    <label for="price">Preis pro Tag</label>
                    <input type="number" name="price" id="price" min="0" max="99999" required>
                    <label for="description">Beschreibung</label>
                    <textarea name="description" rows="3" cols="6" id="description" maxlength="255" required></textarea>
                    <button class="booking-button" type="submit">Absenden</button>
                </div>
                <div class="image-input">
                    <p>Bild hinzufügen</p>
                        <label for="image">+</label>
                        <input id="image" name="image" type="file" accept="image/*" onchange="showPreview(event);" multiple>
                    <div class="image-preview" id="image-preview"></div>
                    <p>${creationResponse}</p>
                </div>
            </form>
    </main>
    <jsp:include page="../includes/footer.jsp"/>
</body>
</html>
