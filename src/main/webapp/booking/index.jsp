<%--Author: Andreas Dinauer--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
    <link rel="stylesheet" type="text/css" href="../styles/global.css">
    <link rel="stylesheet" type="text/css" href="../styles/header.css">
    <link rel="stylesheet" type="text/css" href="../styles/models.css">
    <link rel="stylesheet" type="text/css" href="../styles/booking.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200">
    <script src="../utils/checkout.js"></script>
    <script src="../utils/datepicker.js"></script>
    <title>Buchen - Cars4You</title>
</head>
<body id="app">
<jsp:include page="../includes/header.jsp"/>
<jsp:include page="../SingleListingServlet"/>
    <main class="page">
        <h1 class="space">Buchen: ${currentListing.model.manufacturer}, ${currentListing.model.modelName}</h1>
        <div class="col-2">
            <div>
                <img class="space model-image" alt="car" src="/cars4you/ImageServlet?uuid=${currentListing.imageUuid[0]}">
            </div>
            <div>
                <form action="/cars4you/BookingServlet" method="post">
                    <div class="warning-box" id="warning-box">Ungültiges Datum</div>
                    <c:if test="${param.error != null}">
                        <div class="warning-box show">Fehler bei Buchung aufgetreten!</div>
                    </c:if>
                    <div class="col-2">
                        <div>
                            <label for="booked-from">Von</label>
                            <input id="booked-from" class="date-input" type="date" value="<c:if test="${search.bookedFrom != null}">${search.bookedFrom}</c:if>" name="booked-from">
                        </div>
                        <div>
                            <label for="booked-to">Bis </label>
                            <input id="booked-to" class="date-input" type="date" value="<c:if test="${search.bookedTo != null}">${search.bookedTo}</c:if>" name="booked-to">
                        </div>
                    </div>
                    <div class="checkout-info">
                        <p>Tagespreis</p>
                        <p><span id="daily-price">${currentListing.price}</span> €</p>
                    </div>
                    <div id="total" class="checkout-info">
                        <p>Gesamtbetrag</p>
                        <p><span id="total-value"></span> €</p>
                    </div>
                    <input type="hidden" value="${param.currentListing}" name="listing-id">
                    <button class="center-button" type="submit" id="booking-button">Buchen</button>
                </form>
            </div>
        </div>
        <jsp:include page="../includes/date-picker-popup.jsp"/>
    </main>
<jsp:include page="../includes/footer.jsp"/>
</body>
</html>
