import mysql.connector
import pandas as pd
import uuid
from ast import literal_eval
import argparse
import uuid
import random
import string
from datetime import datetime
from datetime import timedelta
import os
import sampleImages.getSampleImages as getSampleImages
import numpy as np
from threading import Thread, Barrier
import time as t
import undetected_chromedriver as uc

AMOUNT_OF_USERS = 30
AMOUNT_OF_LISTINGS = 100
AMOUNT_OF_BOOKINGS = 100

firstnames = ["Andreas", "Björn", "Carl", "David", "Erik", "Fredrik", "Gustav", "Hans", "Ivar", "Johan", "Karl", "Lars", "Martin", "Nils", "Olof", "Per", "Qvintus", "Rudolf", "Sigurd", "Tore", "Urban", "Viktor", "Wilhelm", "Xerxes", "Yngve", "Zäta"]
lastnames = ["Andersson", "Björk", "Carlsson", "Davidsson", "Eriksson", "Fredriksson", "Gustavsson", "Hansson", "Ivarsson", "Johansson", "Karlsson", "Larsson", "Martinsson", "Nilsson", "Olofsson", "Persson", "Qvintusson", "Rudolfsson", "Sigurdsson", "Tore", "Urban", "Viktorsson", "Wilhelmsson", "Xerxesson", "Yngvesson", "Zätasson"]
fuel_types = ["GASOLINE", "DIESEL", "ELECTRIC", "HYBRID"]
doors = ["2/3", "4/5", "6/7"]
colors = ["Grün", "Gelb", "Rot", "Blau", "Schwarz", "Weiß", "Orange", "Braun", "Grau", "Pink"]
descriptions = ["Super Auto!", "Mein Traumwagen jetzt auch für dich", "Brandneuer Wagen", "Eine elegante und geräumige Mietwagenoption, die Komfort und Stil kombiniert. Sie bietet Platz für bis zu fünf Personen und hat einen großzügigen Kofferraum für Gepäck.", "Bietet Platz für bis zu fünf Personen und garantiert ein luxuriöses Fahrerlebnis.", "Dein Traumwagen zum selbstfahren, Viel Spaß"]


users = []
models = []
listings = []
images = []
bookings = []
## Classes
class User:
    def __init__(self, uuid, firstname, lastname, password, email, joined_date):
        self.uuid = uuid
        self.firstname = firstname
        self.lastname = lastname
        self.password = password
        self.email = email
        self.admin = True
        self.joined_date = joined_date

class Model:
    def __init__(self, uuid, manufacturer, model_name):
        self.uuid = uuid
        self.manufacturer = manufacturer
        self.model_name = model_name

class Listing:
    def __init__(self, uuid, author_id, model_id, manufacturing_year, fuel_type, fuel_capacity, doors, horsepower, seats, color, price, description, created_date):
        self.uuid = uuid
        self.author_id = author_id
        self.model_id = model_id
        self.manufacturing_year = manufacturing_year
        self.fuel_type = fuel_type
        self.fuel_capacity = fuel_capacity
        self.doors = doors
        self.horsepower = horsepower
        self.seats = seats
        self.color = color
        self.price = price
        self.description = description
        self.created_date = created_date

class Image:
    def __init__(self, uuid, image, listing_uuid):
        self.uuid = uuid
        self.image = image
        self.listing_uuid = listing_uuid

class Booking:
    def __init__(self, uuid, listing_uuid, booked_by, booking_datetime, booking_start, booking_end):
        self.uuid = uuid
        self.listing_uuid = listing_uuid
        self.booked_by = booked_by
        self.booking_datetime = booking_datetime
        self.booked_from = booking_start
        self.booked_to = booking_end

### Create tables data ###
def createInitialTables():
    try:
        connection = createConnection()
        cursor = connection.cursor()
        # User table
        sql_create_users_table = """CREATE TABLE IF NOT EXISTS `users` (
                                `uuid` varchar(255) NOT NULL UNIQUE,
                                `firstname` varchar(255) NOT NULL,
                                `lastname` varchar(255) NOT NULL,
                                `password` varchar(255) NOT NULL,
                                `admin` boolean NOT NULL,
                                `email` varchar(255) NOT NULL UNIQUE,
                                `joined_date` date NOT NULL,
                                CONSTRAINT `pk_users` PRIMARY KEY (`uuid`)
                                );"""
        cursor.execute(sql_create_users_table)
        # Models table
        sql_create_models_table = """CREATE TABLE IF NOT EXISTS `models` (
                                    `uuid` varchar(255) NOT NULL UNIQUE,
                                    `manufacturer` varchar(255) NOT NULL,
                                    `model_name` varchar(255) NOT NULL,
                                    CONSTRAINT `pk_models` PRIMARY KEY (`uuid`)
                                );"""
        cursor.execute(sql_create_models_table)
        # Listings table
        sql_create_listings_table = """CREATE TABLE IF NOT EXISTS `listings` (
                                    `uuid` varchar(255) NOT NULL UNIQUE,
                                    `author_id` varchar(255) NOT NULL,
                                    `model_id` varchar(255) NOT NULL,
                                    `manufacturing_year` int(11) NOT NULL,
                                    `fuel_type` varchar(255) NOT NULL,
                                    `fuel_capacity` int(11) NOT NULL,
                                    `doors` varchar(5) NOT NULL,
                                    `horsepower` int(11) NOT NULL,
                                    `seats` int(11) NOT NULL,
                                    `color` varchar(255) NOT NULL,
                                    `price` int(11) NOT NULL,
                                    `description` varchar(255) NOT NULL,
                                    `created_date` date NOT NULL,
                                    CONSTRAINT `fk_listings_author` FOREIGN KEY (`author_id`) REFERENCES `users` (`uuid`),
                                    CONSTRAINT `fk_listings_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`uuid`),
                                    CONSTRAINT `pk_listings` PRIMARY KEY (`uuid`)
                                );"""
        cursor.execute(sql_create_listings_table)
        # Images table
        sql_create_images_table = """CREATE TABLE IF NOT EXISTS `images` (
                                    `uuid` varchar(255) NOT NULL UNIQUE,
                                    `image` LONGBLOB NOT NULL,
                                    `listing_id` varchar(255) NOT NULL,
                                    CONSTRAINT `fk_images_listings` FOREIGN KEY (`listing_id`) REFERENCES `listings` (`uuid`),
                                    CONSTRAINT `pk_images` PRIMARY KEY (`uuid`)
                                );"""
        cursor.execute(sql_create_images_table)
        sql_create_bookings_table = """CREATE TABLE IF NOT EXISTS `bookings` (
                                        `uuid` varchar(255) NOT NULL UNIQUE,
                                        `listing_id` varchar(255) NOT NULL,
                                        `booked_by` varchar(255) NOT NULL,
                                        `booking_datetime` datetime NOT NULL,
                                        `booked_from` date NOT NULL,
                                        `booked_to` date NOT NULL,
                                        CONSTRAINT `fk_bookings_booked_by` FOREIGN KEY (`booked_by`) REFERENCES `users` (`uuid`),
                                        CONSTRAINT `fk_bookings_listings` FOREIGN KEY (`listing_id`) REFERENCES `listings` (`uuid`),
                                        CONSTRAINT `pk_bookings` PRIMARY KEY (`uuid`)
                                    );"""
        cursor.execute(sql_create_bookings_table)
        connection.commit()
        print("Tables created successfully in MySQL")
    except mysql.connector.Error as error:
        print("Failed to create table in MySQL: {}".format(error))
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")

### Generate sample data ###
def createSampleUsers():
    for i in range(AMOUNT_OF_USERS):
        uuid_val = str(uuid.uuid4())
        firstname_val = firstnames[random.randint(0, len(firstnames) - 1)]
        lastname_val = lastnames[random.randint(0, len(lastnames) - 1)]
        password_val = random_string(10)
        email_val = f"{firstname_val}.{lastname_val}{random_string(4)}@cars4you-in.de"
        joined_date_val = random_date(datetime(2020, 1, 1), datetime.now())
        user = User(uuid_val, firstname_val, lastname_val, password_val, email_val, joined_date_val)
        users.append(user)

def createModels():
    df = pd.read_csv('/Users/andreasott/IdeaProjects/car4you/src/main/resources/createSampleData/allModels.csv', index_col=0, sep='|')
    # make list from string as Model row is a list in string format
    df.Modell = df.Modell.apply(literal_eval)
    for index, row in df.iterrows():
        brand = row['Marke']
        for model in row['Modell']:
            uuid_val = str(uuid.uuid4())
            manufacturer_val = brand
            # only take the first 16 characters of the model name
            model_name_val = model[:16]
            model = Model(uuid_val, manufacturer_val, model_name_val)
            models.append(model)

def createSampleListings():
    for i in range(AMOUNT_OF_LISTINGS):
        uuid_val = str(uuid.uuid4())
        author_id_val = users[random.randint(0, len(users) - 1)].uuid
        # get a random model from the list of models
        # do not use the model named "andere"
        random_model = models[random.randint(0, len(models) - 1)]
        while random_model.model_name == "Andere":
            random_model = models[random.randint(0, len(models) - 1)]
        model_id_val = random_model.uuid
        manufacturing_year_val = random.randint(1980, 2020)
        fuel_type_val = fuel_types[random.randint(0, len(fuel_types) - 1)]
        fuel_capacity_val = random.randint(30, 100)
        doors_val = doors[random.randint(0, len(doors) - 1)]
        horsepower_val = random.randint(80, 500)
        seats_val = random.randint(2, 10)
        color_val = colors[random.randint(0, len(colors) - 1)]
        price_val = random.randint(100, 2000)
        description_val = descriptions[random.randint(0, len(descriptions) - 1)]
        created_date_val = random_date(datetime(2020, 1, 1), datetime.now())
        listing = Listing(uuid_val, author_id_val, model_id_val, manufacturing_year_val, fuel_type_val, fuel_capacity_val, doors_val, horsepower_val, seats_val, color_val, price_val, description_val, created_date_val)
        listings.append(listing)

def createSampleImages():
    '''
    # read all images from folder sampleImages
    imagefiles = []
    filepathlist = os.listdir('sampleImages')
    filepathlist.remove('.DS_Store')
    for filename in filepathlist:
        imagefiles.append(open ('sampleImages/' + filename, 'rb').read())
    # create Image objects
    #print(os.listdir('sampleImages'))
    for listing in listings:
        uuid_val = str(uuid.uuid4())
        image_val = imagefiles[random.randint(0, len(imagefiles) - 1)]
        listing_id_val = listing.uuid
        image = Image(uuid_val, image_val, listing_id_val)
        images.append(image)
    for listing in listings:
        uuid_val = str(uuid.uuid4())
        image_val = imagefiles[random.randint(0, len(imagefiles) - 1)]
        listing_id_val = listing.uuid
        image = Image(uuid_val, image_val, listing_id_val)
        images.append(image)
    '''
    
    #splittedList = np.array_split(listings, threads)
    #for i in range(threads):
    #    thread = Thread(target=createSampleImageMultiThreading, args=(splittedList[i].tolist(),))
    #    thread.start()
    #    t.sleep(5)
        


def createSampleBookings():
    for i in range(AMOUNT_OF_BOOKINGS):
        uuid_val = str(uuid.uuid4())
        listing_id_val = listings[random.randint(0, len(listings) - 1)].uuid
        booked_by_val = users[random.randint(0, len(users) - 1)].uuid
        booking_datetime_val = random_date(datetime(2023, 1, 1), datetime.now())
        booked_from_val = random_date(datetime(2023, 1, 1), (datetime.now() - timedelta(days=2)))
        booked_to_val = random_date(booked_from_val, datetime.now())
        booking = Booking(uuid_val, listing_id_val, booked_by_val, booking_datetime_val, booked_from_val, booked_to_val)
        bookings.append(booking)


def createUserForTesting():
    uuid_val = str(uuid.uuid4())
    firstname_val = "Test"
    lastname_val = "User"
    password_val = "test"
    email_val = "test@test.de"
    joined_date_val = random_date(datetime(2020, 1, 1), datetime.now())
    user = User(uuid_val, firstname_val, lastname_val, password_val, email_val, joined_date_val)
    users.append(user)

def createSampleData():
    createSampleUsers()
    createUserForTesting()
    createModels()
    createSampleListings()
    #createSampleImages()
    createSampleBookings()
    

##### Start writing to DB #####
def writeUsersToDB():
    try:
        connection = createConnection()
        cursor = connection.cursor()
        for user in users:
                sql_insert_blob_query = f""" INSERT INTO users
                            (uuid, firstname, lastname, password, admin, email, joined_date) VALUES (%s,%s, %s, %s, %s, %s, %s)"""
                insert_blob_tuple = (user.uuid, user.firstname, user.lastname, user.password, user.admin, user.email, user.joined_date)
                result = cursor.execute(sql_insert_blob_query, insert_blob_tuple)

                #print("User inserted successfully into table", result)
        connection.commit()
        print("Sample Users inserted into db")
        
    except mysql.connector.Error as error:
        print("Failed inserting sample Users into db: {}".format(error))

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")

def writeModelsToDB():
    try:
        connection = createConnection()
        cursor = connection.cursor()
        for model in models:
                sql_insert_blob_query = f""" INSERT INTO models
                            (uuid, manufacturer, model_name) VALUES (%s,%s, %s)"""
                insert_blob_tuple = (model.uuid, model.manufacturer, model.model_name)
                result = cursor.execute(sql_insert_blob_query, insert_blob_tuple)

                #print("Model inserted successfully as a BLOB into images table", result)
        connection.commit()
        print("Sample Models inserted into db")
        
    except mysql.connector.Error as error:
        print("Failed inserting sample models into db: {}".format(error))

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")

def writeListingsToDB():
    try:
        connection = createConnection()
        cursor = connection.cursor()
        for listing in listings:
                sql_insert_blob_query = f""" INSERT INTO listings
                            (uuid, author_id, model_id, manufacturing_year, fuel_type, fuel_capacity, doors, horsepower, seats, color, price, description, created_date) VALUES (%s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s,%s)"""
                insert_blob_tuple = (listing.uuid, listing.author_id, listing.model_id, listing.manufacturing_year, listing.fuel_type, listing.fuel_capacity, listing.doors, listing.horsepower, listing.seats, listing.color, listing.price, listing.description, listing.created_date)
                result = cursor.execute(sql_insert_blob_query, insert_blob_tuple)

                #print("Listing inserted successfully as a BLOB into images table", result)
        connection.commit()
        print("Sample Listings inserted into db")
        
    except mysql.connector.Error as error:
        print(f"Failed inserting sample Listings into db: {error} \n {insert_blob_tuple}")

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")
        
def writeImagesToDB():
    try:
        connection = createConnection()
        cursor = connection.cursor()
        for image in images:
                sql_insert_blob_query = f""" INSERT INTO images
                            (uuid, image, listing_id) VALUES (%s,%s, %s)"""
                insert_blob_tuple = (image.uuid, image.image, image.listing_uuid)
                result = cursor.execute(sql_insert_blob_query, insert_blob_tuple)

                #print("Image inserted successfully as a BLOB into images table", result)
        connection.commit()
        print("Sample Images inserted into db")
        
    except mysql.connector.Error as error:
        print("Failed inserting sample Images into db: {}".format(error))

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")

def writeBookingsToDB():
    try:
        connection = createConnection()
        cursor = connection.cursor()
        for booking in bookings:
                sql_insert_blob_query = f""" INSERT INTO bookings
                            (uuid, listing_id, booked_by, booking_datetime, booked_from, booked_to) VALUES (%s,%s, %s, %s, %s, %s)"""
                insert_blob_tuple = (booking.uuid, booking.listing_uuid, booking.booked_by, booking.booking_datetime, booking.booked_from, booking.booked_to)
                result = cursor.execute(sql_insert_blob_query, insert_blob_tuple)

                #print("Booking inserted successfully as a BLOB into images table", result)
        connection.commit()
        print("Sample Bookings inserted into db")
        
    except mysql.connector.Error as error:
        print("Failed inserting sample Bookings into db: {}".format(error))

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")



def writeToDB():
    writeUsersToDB()
    writeModelsToDB()
    writeListingsToDB()
    #writeImagesToDB()
    writeBookingsToDB()



# Util Functions
# Create a connection to the database
def createConnection():
    connection = None
    if(args.password == "none"):
        connection = mysql.connector.connect(host=args.host,
                                            database='thidb',
                                            user='root')
    else:
        connection = mysql.connector.connect(host=args.host,
                                            database='thidb',
                                            user='root',
                                            password=args.password)
    return connection


# Generate a random string of characters
def random_string(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

# Generate a random date between two dates
def random_date(start_date, end_date):
    time_between = end_date - start_date
    days_between = time_between.days
    random_number_of_days = random.randrange(days_between)
    random_date = start_date + timedelta(days=random_number_of_days)
    return random_date


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    # input firstrun boolean
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--password', type=str, default="none")

    args = parser.parse_args()
    createInitialTables()
    createSampleData()
    writeToDB()


