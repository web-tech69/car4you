from bs4 import BeautifulSoup
import requests
from selenium import webdriver
import time as t
import undetected_chromedriver as uc
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import numpy as np
from selenium.webdriver.common.keys import Keys
import shutil
from PIL import Image
from io import BytesIO
from random import randint

searchConsole = '//*[@id="APjFqb"]'

def createInitialSession(driver: uc.Chrome):

    driver.get('https://www.google.com/imghp')
    cookieAccept = '//*[@id="L2AGLb"]/div'
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
        (By.XPATH, cookieAccept)))
    t.sleep(1)
    try:
        driver.find_element("xpath", cookieAccept).click()
        t.sleep(1)
    except:
        print("No cookie accept button found")
        exit(1)
    
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
        (By.XPATH, searchConsole)))
    

def downloadImageToFile(src: str):
    image_filename = src.split("/")[-1]
    r = requests.get(src, stream = True)
    if r.status_code == 200:
        # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
        r.raw.decode_content = True
        
        # Open a local file with wb ( write binary ) permission.
        with open(image_filename,'wb') as f:
            shutil.copyfileobj(r.raw, f)
        
        print('Image sucessfully Downloaded: ',image_filename)
    else:
        print('Image Couldn\'t be retreived')

def downloadImageToVar(src: str) -> bytes | None:
    try:
        r = requests.get(src, stream = True)
    except requests.exceptions.RequestException as e:
        print(e)
        return None
    if r.status_code == 200:
        try:
            image = Image.open(BytesIO(r.content))
        except Exception as e:
            print(e)
            return None

        output = BytesIO()
        image.save(output, format="JPEG", optimize=True, quality=40)
        return output.getvalue()
    else:
        print('Image Couldn\'t be retreived')

def main(driver):
    #driver = uc.Chrome()
    if(str(driver.current_url).startswith('chrome://')):
        print("Creating initial session")
        createInitialSession(driver)
    else:
        print("Session already created")
        driver.get('https://www.google.com/imghp')
    driver.find_element("xpath", searchConsole).send_keys("Mercedes Benz C230 Orange")
    driver.find_element("xpath", searchConsole).send_keys(Keys.ENTER)
    #t.sleep(2)
    firstPicture = '//*[@id="islrg"]/div[1]/div[1]/a[1]/div[1]/img'
    allPictures = '//*[@id="islrg"]//*/img[not(@data-sz)]'
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
        (By.XPATH, firstPicture)))
    allPicturesElements = driver.find_elements("xpath", allPictures)
    counter = 0
    for picture in allPicturesElements:
        picture.click()
        firstPictureBigger = '//*[@id="Sva75c"]/div[2]/div[2]/div[2]/div[2]/c-wiz/div/div/div/div[3]/div[1]/a/img[1]'
        t.sleep(2)
        img = driver.find_element("xpath", firstPictureBigger)
        src = img.get_attribute('src')
        if(not(src.endswith('jpg'))):
            continue
        print(src)
        downloadImageToFile(src)
        counter += 1
        if counter == 3:
            break
    t.sleep(2)


def get3SampleImages(driver: uc.Chrome, Brand: str, Model: str, Color: str) -> list:
    searchText = Brand + " " + Model + " " + Color
    if(str(driver.current_url).startswith('chrome://')):
        createInitialSession(driver)
    else:
        driver.get('https://www.google.com/imghp')
    print("Searching for: " + searchText)
    t.sleep(randint(1,2))
    driver.find_element("xpath", searchConsole).send_keys(searchText)
    t.sleep(randint(1,2))
    driver.find_element("xpath", searchConsole).send_keys(Keys.ENTER)
    allPictures = '//*[@id="islrg"]//*/img[not(@data-sz) and not(@class="zRyhuf")]'
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
        (By.XPATH, allPictures)))
    allPicturesElements = driver.find_elements("xpath", allPictures)
    counter = 0
    images = []
    for picture in allPicturesElements:
        firstPictureBigger = '//*/img[@class="r48jcc pT0Scc iPVvYb"]'
        try:
            t.sleep(randint(1,2))
            picture.click()
            t.sleep(2.5)
            img = driver.find_element("xpath", firstPictureBigger)
        except Exception as e:
            print(f"No bigger picture found {e}")
            continue
        try:
            img = driver.find_element("xpath", firstPictureBigger)
            src = img.get_attribute('src')
        except Exception as e:
            print(f"No src found {e}")
            continue
        if(not(src.endswith('jpg'))):
            continue
        image = downloadImageToVar(src)
        if(image != None):
            images.append(image)
            counter += 1
        else:
            continue
        if counter == 3:
            break
    return images

if __name__=='__main__':
    driver = uc.Chrome()
    images = get3SampleImages(driver, "Mercedes Benz", "C230", "Orange")
    # write images to file
    index = 0
    for image in images:
        with open(f"test{index}.jpg", "wb") as file:
            file.write(image)
        index += 1

    driver.close()



