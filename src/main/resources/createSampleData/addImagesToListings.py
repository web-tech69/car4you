import mysql.connector
import undetected_chromedriver as uc
import sampleImages.getSampleImages as getSampleImages
import uuid
import argparse

dbHost = "localhost"
password = "cars4youDB"



def main():
    listingsUuid = getListingsUUIDs()
    # check which listings have no images
    listingsWithImages = getListingsUUIDsWithImages()
    for listing in listingsWithImages:
        try:
            listingsUuid.remove(listing)
        except:
            print("listinguuid: " + listing)
    driver = uc.Chrome()
    for listing in listingsUuid:
        listingData = getListingData(listing)
        print("Listing:")
        print(listingData)
        if(listingData != None):
            createSampleImage(listing, listingData[1], listingData[0], listingData[2], driver)
    driver.close()
        

    
    
def getListingData(listing: str) -> list | None:
    try:
        connection = createConnection()
        cursor = connection.cursor()
        sql_select_query = f"""SELECT color FROM listings WHERE uuid = '{listing}'"""
        cursor.execute(sql_select_query)
        records = cursor.fetchone()
        if(records != None):
            color = records[0]
        else:
            color = ""
        sql_select_query = f"""SELECT model_id FROM listings WHERE uuid = '{listing}'"""
        cursor.execute(sql_select_query)
        records = cursor.fetchone()
        if(records != None):
            model_id = records[0]
            sql_select_query = f"""SELECT * FROM models WHERE uuid = '{model_id}'"""
            cursor.execute(sql_select_query)
            records = cursor.fetchone()
            if(records != None):
                model_name = records[1]
                manufacturer = records[2]
                return [manufacturer, model_name, color]
    except mysql.connector.Error as error:
        print("Failed to get record from MySQL table: {}".format(error))
        return None, None, None
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


def getListingsUUIDsWithImages() -> list:
    try:
        connection = createConnection()
        cursor = connection.cursor()
        sql_select_query = f"""SELECT listing_id FROM images"""
        cursor.execute(sql_select_query)
        records = cursor.fetchall()
        listings = []
        for row in records:
            listings.append(row[0])
        print("Total number of rows in listings is: ", cursor.rowcount)
        return listings
    except mysql.connector.Error as error:
        print("Failed to get record from MySQL table: {}".format(error))
        return []
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


def getListingsUUIDs() -> list:
    try:
        connection = createConnection()
        cursor = connection.cursor()
        sql_select_query = """SELECT uuid FROM listings"""
        cursor.execute(sql_select_query)
        records = cursor.fetchall()
        listings = []
        for row in records:
            listings.append(row[0])
        print("Total number of rows in listings is: ", cursor.rowcount)
        return listings
    except mysql.connector.Error as error:
        print("Failed to get record from MySQL table: {}".format(error))
        return []
    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


def createSampleImage(listinguuid, brand, model, color, driver):
    try:
        imagefiles = getSampleImages.get3SampleImages(driver, brand, model, color)
    except Exception as e:
        print(f"Error getting images: {e}")
        imagefiles = []
        pass

    for image in imagefiles:
        uuid_val = str(uuid.uuid4())
        print(type(image))
        if(image == None):
            continue
        try:
            connection = createConnection()
            cursor = connection.cursor()
            sql_insert_blob_query = f""" INSERT INTO images
                            (uuid, image, listing_id) VALUES (%s,%s, %s)"""
            insert_blob_tuple = (uuid_val, image, listinguuid)
            result = cursor.execute(sql_insert_blob_query, insert_blob_tuple)

                #print("Image inserted successfully as a BLOB into images table", result)
            connection.commit()
            print("Sample Images inserted into db")
        
        except mysql.connector.Error as error:
            print("Failed inserting sample Images into db: {}".format(error))

        finally:
            if connection.is_connected():
                cursor.close()
                connection.close()
                print("MySQL connection is closed")


def createConnection():
    connection = None
    if(args.password == "none"):
        connection = mysql.connector.connect(host=args.host,
                                            database='thidb',
                                            user='root')
    else:
        connection = mysql.connector.connect(host=args.host,
                                            database='thidb',
                                            user='root',
                                            password=args.password)
    return connection


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # input firstrun boolean
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--password', type=str, default="none")
    args = parser.parse_args()
    main()