from unicodedata import name
from bs4 import BeautifulSoup
import requests
from selenium import webdriver
import selenium
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import pandas as pd
import time as t
import undetected_chromedriver as uc
from threading import Thread
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
import argparse
import numpy as np
import logging
from tqdm import tqdm



def createInitialSession(driver):

    driver.get('https://www.mobile.de/')
    WebDriverWait(driver, 60).until(EC.element_to_be_clickable(
        (By.XPATH, '//*[@id="mde-consent-modal-container"]/div[2]/div[2]/div[1]/button')))
    driver.find_element("xpath", '//*[@id="mde-consent-modal-container"]/div[2]/div[2]/div[1]/button').click()
    # //*[@id="mde-consent-modal-container"]/div[2]/div[2]/div[1]/button]
    #WebDriverWait(driver, 60).until(EC.element_to_be_clickable(
    #            (By.XPATH, '//*[@id="uc-btn-accept-banner"]')))
    #driver.find_element("xpath", '//*[@id="uc-btn-accept-banner"]').click()

    t.sleep(1)


def main(firstrun: bool, threads: int):
    global df
    # First Run load from CSV
    df = None
    
    if(firstrun == True):
        driver = uc.Chrome()
        print("First Run")
        df = pd.DataFrame()
        df['Marke'] = "nan"
        df['Modell'] = "nan"
        df['Scraped'] = "False"
        createInitialSession(driver)
        getBrandFromMobile(driver)
        driver.close()
    else:
        print("Not First Run")
        df = pd.read_csv('allModels.csv', index_col=0, sep='|')
        #df = pd.read_pickle("testDataMobile")
    
    # create Threads
    listOfUnscrapedArticles = df.index[df['Scraped'] == "False"].tolist()

    print(df.head())
    print("Start Scraping")


    df.to_csv("allModels.csv", sep='|')
    # Split List into Threads
    splittedList = np.array_split(df.index.to_list(), threads)
    for i in range(threads):
        thread = Thread(target=scrapeModelsWithPercentage, args=(splittedList[i].tolist(),))
        thread.start()
        t.sleep(1)
    
    #for index in df.index:
    #    getModelFromBrand(driver, index)

    


    
def scrapeModelsWithPercentage(chunkOfUnscrapedTargets: list):
    global df
    driver = uc.Chrome()
    createInitialSession(driver)
    for index in tqdm(chunkOfUnscrapedTargets):
        logging.info(f"Index: {index}, value: {df['Marke'].loc[index]}")
        # Select Brand
        try:
            getModelFromBrand(driver, index)
        except Exception as e:
            logging.error(f"Model: {df['Marke'].loc[index]} coruppted \n execption:{e}")
            continue

    driver.close()


def getBrandFromMobile(driver):
    global df
    # //*/select/optgroup[2]/*
    # better: //*/select/optgroup[contains(@label, 'Alle Marken')]/*
    # for models: //*/select[contains(@data-testid, 'qs-select-model')]/*
    brandsListSeleniumElements = driver.find_elements("xpath", "//*/select/optgroup[contains(@label, 'Alle Marken')]/*")
    brandsList = []
    for brand in brandsListSeleniumElements:
        brandsList.append(brand.text)
    #print(brandsList)
    #print(type(brandsList))
    # insert brands into df
    df["Marke"]= brandsList

def getModelFromBrand(driver, brandIndex):
    global df
    currentBrand = df["Marke"].loc[brandIndex]
    # select brand
    driver.find_element("xpath", f"//*/select/optgroup[2]/option[text()='{currentBrand}']").click()
    t.sleep(1)
    # get models
    modelsListSeleniumElements = driver.find_elements("xpath", f"//*/select[contains(@data-testid, 'qs-select-model')]/*")
    modelsList = []
    for model in modelsListSeleniumElements:
        if(model.text.__contains__("\n")):
            modelNewLineSplit = model.text.split("\n")
            print(modelNewLineSplit)
            for modelNewLine in modelNewLineSplit:
                if(not(modelNewLine.__contains__("(Alle)"))):
                    modelsList.append(modelNewLine)
        elif(model.text != "Beliebig"): modelsList.append(model.text)


    logging.info(f"Brand: {currentBrand}, Models: {modelsList}")
    logging.debug("Models: " + str(modelsList))
    # insert models into df
    
    df["Modell"].loc[brandIndex] = modelsList
    #df.at[1, 'Modell'] = modelsList
    df.to_csv("allModels.csv", sep='|')



if __name__=='__main__':
    # process args
    logging.basicConfig(filename='scraper.log', encoding='utf-8', level=logging.INFO)
    parser = argparse.ArgumentParser()
    # input firstrun boolean
    parser.add_argument('--firstrun', type=bool, default=False)
    parser.add_argument('--threads', type=int, default=1)

    args = parser.parse_args()

    #main(firstrun=args.firstrun, threads=args.threads)
    main(firstrun=args.firstrun, threads=args.threads)