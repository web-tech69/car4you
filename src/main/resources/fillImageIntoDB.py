# Replaced by sampleDataPy.py

import mysql.connector
import base64
import binascii


def convertToBinaryData(filename):
    # Convert digital data to binary format
    with open(filename, 'rb') as file:
        binaryData = file.read()
    return binaryData




def insertBLOB(uuid, photo, listing_id):
    print("Inserting BLOB into images table")
    try:
        connection = mysql.connector.connect(host='localhost',
                                             database='thidb',
                                             user='root')

        cursor = connection.cursor()
        

        file  = convertToBinaryData(photo)
        #file = base64.b64encode(file)
        #file = binascii.hexlify(file).decode('utf-8')
        sql_insert_blob_query = f""" INSERT INTO images
                          (uuid, image, listing_id) VALUES (%s,%s,%s)"""
        insert_blob_tuple = (uuid, file, listing_id)
        result = cursor.execute(sql_insert_blob_query, insert_blob_tuple)
        connection.commit()
        print("Image and file inserted successfully as a BLOB into images table", result)

    except mysql.connector.Error as error:
        print("Failed inserting BLOB data into images table {}".format(error))

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")


if __name__ == '__main__':
    insertBLOB('dsad', '/Users/andreasott/Documents/fotos/SkodaEniaq.png', 'ijkl')
    insertBLOB('djas', '/Users/andreasott/Documents/fotos/SkodaEniaq.png', 'mnop')
    insertBLOB('qrst', '/Users/andreasott/Documents/fotos/Lotus.jpeg', 'ijkl')
    insertBLOB('uvwx', '/Users/andreasott/Documents/fotos/Lotus.jpeg', 'mnop')
