# Replaced by sampleDataPy.py

import mysql.connector
import pandas as pd
import uuid
from ast import literal_eval
import argparse



def writeDataToDB():
    try:
        connection = None
        if(args.password == "none"):
            connection = mysql.connector.connect(host=args.host,
                                                database='thidb',
                                                user='root')
        else:
            connection = mysql.connector.connect(host=args.host,
                                                database='thidb',
                                                user='root',
                                                password=args.password)
        cursor = connection.cursor()
        df = pd.read_csv('allModels.csv', index_col=0, sep='|')
        # make list from string as Model row is a list in string format
        df.Modell = df.Modell.apply(literal_eval)
        for index, row in df.iterrows():
            brand = row['Marke']
            for model in row['Modell']:
                sql_insert_blob_query = f""" INSERT INTO models
                            (uuid, manufacturer, model_name) VALUES (%s,%s, %s)"""
                insert_blob_tuple = (str(uuid.uuid4()), brand, model)
                result = cursor.execute(sql_insert_blob_query, insert_blob_tuple)

                print("Model inserted successfully as a BLOB into images table", result)
        connection.commit()
        
    except mysql.connector.Error as error:
        print("Failed inserting BLOB data into images table {}".format(error))

    finally:
        if connection.is_connected():
            cursor.close()
            connection.close()
            print("MySQL connection is closed")




if __name__=='__main__':
    parser = argparse.ArgumentParser()
    # input firstrun boolean
    parser.add_argument('--host', type=str, default="localhost")
    parser.add_argument('--password', type=str, default="none")

    args = parser.parse_args()
    writeDataToDB()