# Commands to execute python files:

## Install requirements:
``` shell
python3 -m pip install -r requirements.txt
```

## Webscraper:
### To run the webscraper, run the following command:
``` shell
python3 scrapeAllCarmodels.py --firstrun True --threads 5
```
### Generate Example and write init.sql to database
With password:
``` shell
# local
python3 sampleDataPy.py --host localhost --password password
# remote
python3 sampleDataPy.py --host cars4you-in.de --password password
```
Without password:
``` shell
# local
python3 sampleDataPy.py --host localhost
# remote
python3 sampleDataPy.py --host cars4you-in.de
```

## Write images to database: Not needed anymore
``` shell
python3 fillImageIntoDB.py
```