#  Author: Johannes Buchberger
CREATE TABLE IF NOT EXISTS `users` (
  `uuid` varchar(255) NOT NULL UNIQUE,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL UNIQUE,
  `admin` varchar(255) NOT NULL,
  `joined_date` date NOT NULL,
  CONSTRAINT `pk_users` PRIMARY KEY (`uuid`)
);

CREATE TABLE IF NOT EXISTS `models` (
    `uuid` varchar(255) NOT NULL UNIQUE,
    `manufacturer` varchar(255) NOT NULL,
    `model_name` varchar(255) NOT NULL,
    CONSTRAINT `pk_models` PRIMARY KEY (`uuid`)
);


CREATE TABLE IF NOT EXISTS `listings` (
    `uuid` varchar(255) NOT NULL UNIQUE,
    `author_id` varchar(255) NOT NULL,
    `model_id` varchar(255) NOT NULL,
    `manufacturing_year` int(11) NOT NULL,
    `fuel_type` varchar(255) NOT NULL,
    `fuel_capacity` int(11) NOT NULL,
    `doors` varchar(5) NOT NULL,
    `horsepower` int(11) NOT NULL,
    `seats` int(11) NOT NULL,
    `color` varchar(255) NOT NULL,
    `price` int(11) NOT NULL,
    `description` varchar(255) NOT NULL,
    `created_date` date NOT NULL,
    CONSTRAINT `fk_listings_author` FOREIGN KEY (`author_id`) REFERENCES `users` (`uuid`),
    CONSTRAINT `fk_listings_model` FOREIGN KEY (`model_id`) REFERENCES `models` (`uuid`),
    CONSTRAINT `pk_listings` PRIMARY KEY (`uuid`)
);

CREATE TABLE IF NOT EXISTS `images` (
    `uuid` varchar(255) NOT NULL UNIQUE,
    `image` LONGBLOB NOT NULL,
    `listing_id` varchar(255) NOT NULL,
    CONSTRAINT `fk_images_listings` FOREIGN KEY (`listing_id`) REFERENCES `listings` (`uuid`),
    CONSTRAINT `pk_images` PRIMARY KEY (`uuid`)
);

CREATE TABLE IF NOT EXISTS `bookings` (
    `uuid` varchar(255) NOT NULL UNIQUE,
    `listing_id` varchar(255) NOT NULL,
    `booked_by` varchar(255) NOT NULL,
    `booking_datetime` datetime NOT NULL,
    `booked_from` date NOT NULL,
    `booked_to` date NOT NULL,
    CONSTRAINT `fk_bookings_booked_by` FOREIGN KEY (`booked_by`) REFERENCES `users` (`uuid`),
    CONSTRAINT `fk_bookings_listings` FOREIGN KEY (`listing_id`) REFERENCES `listings` (`uuid`),
    CONSTRAINT `pk_bookings` PRIMARY KEY (`uuid`)
);


-- Inserting values into the users table
INSERT INTO `users` (`uuid`, `firstname`, `lastname`, `password`, `email`, `joined_date`, `admin`)
VALUES  ('e162cf3e-0ea7-47e6-8b05-0dba65eb39ca', 'John', 'Doe', '123', 'john@doe.com', '2021-01-01', false),
        ('3b8abdc3-2ee2-423a-87bc-905a7f963a3c', 'Jane', 'Doe', '123', 'jane@doe.com', '2021-01-01', true);

-- Inserting values into the models table
INSERT INTO `models` (`uuid`, `manufacturer`, `model_name`)
VALUES  ('bab3f324-c8c1-47b2-82af-ea7dce6b7fb9', 'Toyota', 'Corolla'),
        ('ed1b4a33-4ab1-4b0b-8917-aaad42a35641', 'Honda', 'Civic'),
        ('5ed35f92-6b25-46a6-ad7c-9ddc6d171d3a', 'Audi', 'A3'),
        ('07ee636e-e190-410b-b73b-83c1d56fbdbb', 'BMW', 'i8');

-- Inserting values into the listings table
INSERT INTO `listings` (`uuid`, `author_id`, `model_id`, `manufacturing_year`, `price`, `color`, `fuel_type`, `fuel_capacity`, `doors`, `horsepower`, `seats`, `description`, `created_date`)
VALUES ('1bd62ff2-c56c-41e3-93f3-993bdf7f3f9c', 'e162cf3e-0ea7-47e6-8b05-0dba65eb39ca', 'bab3f324-c8c1-47b2-82af-ea7dce6b7fb9', 2018, 15000, 'Blue', 'GASOLINE', 50, '4', 140, 5, 'Great condition, low mileage', '2021-01-01'),
       ('be7c53e2-860b-4f5e-b0f8-bb5c9fb8e14e', 'e162cf3e-0ea7-47e6-8b05-0dba65eb39ca', 'ed1b4a33-4ab1-4b0b-8917-aaad42a35641', 2019, 17000, 'Red Metallic', 'GASOLINE', 45, '4', 130, 4, 'Like new, only driven on weekends', '2021-01-01'),
       ('3cda5121-2b65-42f4-b79f-78f07e66d8da', '3b8abdc3-2ee2-423a-87bc-905a7f963a3c', '5ed35f92-6b25-46a6-ad7c-9ddc6d171d3a', 2006, 17000, 'Red', 'GASOLINE', 35, '2', 150, 4, 'This car sucks, so I sell it', '2021-01-01'),
       ('9a830e95-972d-4c1e-88d4-3d27aa539e8d', '3b8abdc3-2ee2-423a-87bc-905a7f963a3c', '07ee636e-e190-410b-b73b-83c1d56fbdbb', 2018, 17000, 'Green', 'ELECTRIC', 65, '4', 120, 2, 'Best car Ive ever owned.', '2021-01-01'),
       ('f82316ae-5781-4f77-948f-31a097bd252c', '3b8abdc3-2ee2-423a-87bc-905a7f963a3c', 'ed1b4a33-4ab1-4b0b-8917-aaad42a35641', 2013, 17000, 'Dark Red', 'GASOLINE', 45, '2', 130, 5, 'I am selling this car, because I am buying a Ferrari soon. Is in great condition', '2021-01-01');


-- Inserting values into the bookings table
INSERT INTO `bookings` (`uuid`, `listing_id`, `booked_by`, `booking_datetime`, `booked_from`, `booked_to`)
VALUES  ('7cd0bdfc-aa7b-481f-8ad9-41972e065021', '1bd62ff2-c56c-41e3-93f3-993bdf7f3f9c', 'e162cf3e-0ea7-47e6-8b05-0dba65eb39ca', '2021-01-01 10:00:00', '2021-02-01', '2021-02-05'),
        ('aa798f08-bf35-493a-aad6-5d942f019a57', '1bd62ff2-c56c-41e3-93f3-993bdf7f3f9c', 'e162cf3e-0ea7-47e6-8b05-0dba65eb39ca', '2021-01-01 11:00:00', '2021-02-10', '2021-02-15'),
        ('736ced5e-050a-4a83-88ea-f871bd1030fe', '1bd62ff2-c56c-41e3-93f3-993bdf7f3f9c', 'e162cf3e-0ea7-47e6-8b05-0dba65eb39ca', '2021-01-01 11:00:00', '2021-02-17', '2021-02-20'),
        ('e9c28eb3-4ba4-47a3-868c-7087ff547f6e', 'be7c53e2-860b-4f5e-b0f8-bb5c9fb8e14e', 'e162cf3e-0ea7-47e6-8b05-0dba65eb39ca', '2021-01-01 11:00:00', '2021-02-10', '2021-02-15'),
        ('9ddb530f-b1eb-4e53-bf10-e3dbf2a6ad87', 'be7c53e2-860b-4f5e-b0f8-bb5c9fb8e14e', '3b8abdc3-2ee2-423a-87bc-905a7f963a3c', '2021-01-01 11:00:00', '2021-02-19', '2021-02-24'),
        ('c2b375d6-960e-4f45-a0a3-9662c27dfeea', 'be7c53e2-860b-4f5e-b0f8-bb5c9fb8e14e', '3b8abdc3-2ee2-423a-87bc-905a7f963a3c', '2021-01-01 11:00:00', '2021-02-27', '2021-03-03');

-- Inserting values into the images table
-- INSERT INTO `images` (`uuid`, `image`, `listing_id`)
-- VALUES ('qrst', LOAD_FILE('/Users/andreasott/Documents/fotos/CUPRA.jpg'), 'ijkl'),
--       ('uvwx', LOAD_FILE('/Users/andreasott/Documents/fotos/CUPRA.jpg'), 'mnop');