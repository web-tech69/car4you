// Author: Andreas Dinauer
// Data Transfer Object: Daten, welche auf der Buchungsseite der Account Seite verwendet werden (z.B. Anzahl Tage der Buchung,
// Gesamtpreis) sind nicht in der JPA Entity enthalten, und müssen somit serverseitig vorberechnet werden um mit Hilfe
// dieses DTOs im Frontend angezeigt zu werden.

package de.in.cars4you.dto;

import de.in.cars4you.jpa.entity.Booking;
import de.in.cars4you.jpa.entity.Listing;

import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class BookingDTO {

    private Listing listing;
    private String bookedFrom;
    private String bookedTo;
    private String bookingDatetime;
    private int days;
    private int priceTotal;

    public BookingDTO(Booking booking) {
        this.listing = booking.getListing();
        this.bookedFrom = formatDate(booking.getBookedFrom());
        this.bookedTo = formatDate(booking.getBookedTo());
        this.days = countDays(booking);
        this.bookingDatetime = formatDatetime(booking.getBookingDatetime());
        this.priceTotal = this.days * listing.getPrice();
    }

    private String formatDate(LocalDate date) {
        Locale locale = new Locale("de");
        return date.getDayOfWeek().getDisplayName(TextStyle.SHORT, locale) + " " + date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }

    private String formatDatetime(LocalDateTime dateTime) {
        Locale locale = new Locale("de");
        return dateTime.getDayOfWeek().getDisplayName(TextStyle.SHORT, locale) + " " + dateTime.format(DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm")) + " Uhr";
    }

    private int countDays(Booking booking) {
        List<LocalDate> days = booking.getBookedFrom().datesUntil(booking.getBookedTo()).collect(Collectors.toList());
        return days.size() + 1;
    }

    public Listing getListing() {
        return listing;
    }

    public String getBookedFrom() {
        return bookedFrom;
    }

    public String getBookedTo() {
        return bookedTo;
    }

    public int getDays() {
        return days;
    }

    public int getPriceTotal() {
        return priceTotal;
    }

    public String getBookingDatetime() {
        return bookingDatetime;
    }
}
