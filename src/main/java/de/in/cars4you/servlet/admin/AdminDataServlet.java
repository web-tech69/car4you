/**
 * @Author: Andreas Konrad Ott
 */

package de.in.cars4you.servlet.admin;

import de.in.cars4you.jpa.entity.Listing;
import de.in.cars4you.jpa.entity.User;
import de.in.cars4you.jpa.repo.user.ListingRepo;
import de.in.cars4you.jpa.repo.user.UserRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet("AdminDataServlet")
public class AdminDataServlet extends HttpServlet {
    @Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        // Check if User is admin:
        User user = (User) session.getAttribute("user");
        if (user == null || !user.isAdmin()) {
            session.setAttribute("globalMenuSelection", 0);
            response.sendRedirect(request.getContextPath()+ "/login");
            return;
        }

        // get all models and users
        try (Connection connection = dataSource.getConnection()) {
            List<User> users = new UserRepo(connection).findAll();
            session.setAttribute("users", users);
            List<Listing> listings = new ListingRepo(connection).getAllListings();
            session.setAttribute("listings", listings);
            session.setAttribute("globalMenuSelection", 1);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }

        response.sendRedirect("/cars4you/admin");
    }
}
