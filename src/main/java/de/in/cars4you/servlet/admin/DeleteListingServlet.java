/**
 * @Author: Andreas Ott
 */

package de.in.cars4you.servlet.admin;

import de.in.cars4you.jpa.entity.Listing;
import de.in.cars4you.jpa.repo.booking.BookingRepo;
import de.in.cars4you.jpa.repo.user.ImageRepo;
import de.in.cars4you.jpa.repo.user.ListingRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet("DeleteListingServlet")
public class DeleteListingServlet extends HttpServlet{
    @Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String listingId = request.getParameter("listingId");
        // Delete Bookings of Listing
        try (Connection connection = dataSource.getConnection()) {
            new BookingRepo(connection).deleteByListingUuid(listingId);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // Delete Image of Listing
        try (Connection connection = dataSource.getConnection()) {
            new ImageRepo(connection).deleteByListingUuid(listingId);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // Delete Listing
        try (Connection connection = dataSource.getConnection()) {
            new ListingRepo(connection).deleteByUuid(listingId);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        response.sendRedirect("/cars4you/AdminDataServlet");
    }
}
