/**
 * @Author: Andreas Ott
 */

package de.in.cars4you.servlet.admin;

import de.in.cars4you.jpa.entity.Listing;
import de.in.cars4you.jpa.repo.booking.BookingRepo;
import de.in.cars4you.jpa.repo.user.ImageRepo;
import de.in.cars4you.jpa.repo.user.ListingRepo;
import de.in.cars4you.jpa.repo.user.UserRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet("DeleteUserServlet")
public class DeleteUserServlet extends HttpServlet {
    @Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String userId = request.getParameter("userId");

        try (Connection connection = dataSource.getConnection()) {
            // Get Listings of User and delete their bookings
            ListingRepo listingRepo = new ListingRepo(connection);
            List<Listing> listings = listingRepo.findByAuthor(userId);
            BookingRepo bookingRepo = new BookingRepo(connection);
            for (Listing listing : listings) {
                bookingRepo.deleteByListingUuid(listing.getUuid());
            }
            // Delete Bookings of User
            bookingRepo.deleteByUserUuid(userId);
            // Delete Images of user
            for (Listing listing : listings) {
                new ImageRepo(connection).deleteByListingUuid(listing.getUuid());
            }
            // Delete Listings of User
            listingRepo.deleteByUserUuid(userId);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // Delete The user
        try (Connection connection = dataSource.getConnection()) {
            new UserRepo(connection).deleteByUuid(userId);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        response.sendRedirect("/cars4you/AdminDataServlet");

    }
}
