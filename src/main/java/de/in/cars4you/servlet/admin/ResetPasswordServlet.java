/**
 * @Author: Andreas Ott
 */

package de.in.cars4you.servlet.admin;

import de.in.cars4you.jpa.entity.User;
import de.in.cars4you.jpa.repo.user.UserRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;

@WebServlet("ResetPasswordServlet")
public class ResetPasswordServlet extends HttpServlet {
    @Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String newPassword = request.getParameter("newPassword");
        String userId = request.getParameter("userId");
        // Change password to "password"
        try (Connection connection = dataSource.getConnection()) {
            UserRepo userrepo = new UserRepo(connection);
            User user = userrepo.findByUuid(userId);
            user.setPassword(newPassword);
            userrepo.update(user);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        response.sendRedirect("/cars4you/AdminDataServlet");
    }
}
