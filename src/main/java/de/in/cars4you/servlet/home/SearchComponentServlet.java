/**
 * @Author: Andreas Dinauer
 */
package de.in.cars4you.servlet.home;

import de.in.cars4you.jpa.repo.user.ModelRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet("SearchComponentServlet")
public class SearchComponentServlet extends HttpServlet {

    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try(Connection con = dataSource.getConnection()) {
            ModelRepo modelRepo = new ModelRepo(con);
            request.setAttribute("brands", modelRepo.getAllBrands());
            response.sendRedirect(request.getContextPath() + "/");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
