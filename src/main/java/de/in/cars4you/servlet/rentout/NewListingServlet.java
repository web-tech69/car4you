/**
 * @Author: Andreas Ott
 */
package de.in.cars4you.servlet.rentout;

import de.in.cars4you.jpa.entity.Image;
import de.in.cars4you.jpa.entity.Listing;
import de.in.cars4you.jpa.entity.Model;
import de.in.cars4you.jpa.entity.User;
import de.in.cars4you.jpa.repo.user.ImageRepo;
import de.in.cars4you.jpa.repo.user.ListingRepo;
import de.in.cars4you.jpa.repo.user.ModelRepo;
import de.in.cars4you.utils.FuelType;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

@WebServlet("NewListingServlet")
@MultipartConfig()
public class NewListingServlet extends HttpServlet {

    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        request.setCharacterEncoding("UTF-8");
        String brandName = request.getParameter("brands");
        String modelName = request.getParameter("model");

        String price = request.getParameter("price");
        String color = request.getParameter("color");
        String description = request.getParameter("description");
        int manufacturingYear = parseInt(request.getParameter("year"));
        FuelType fuelType = FuelType.valueOf(request.getParameter("fuelType"));
        int fuelCapacity = parseInt(request.getParameter("fuelCapacity"));
        int horsepower = parseInt(request.getParameter("horsepower"));
        String doors = request.getParameter("doors");
        int seats = parseInt(request.getParameter("seats"));
        // get images from request formdata

        List<String> images = new ArrayList<>();
        int i = 0;
        while (request.getParameter("image" + i) != null)
        {
            images.add(request.getParameter("image" + i));
            i++;
        }
        // create listing
        Listing newListing = new Listing();
        newListing.setUuid();
        newListing.setPrice(parseInt(price));
        newListing.setDescription(description);
        newListing.setManufacturingYear(manufacturingYear);
        newListing.setColor(color);
        newListing.setFuelCapacity(fuelCapacity);
        newListing.setFuelType(FuelType.GASOLINE);
        newListing.setDoors(doors);
        newListing.setHorsepower(horsepower);
        newListing.setSeats(seats);
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        newListing.setAuthor(user);
        // create Images and add them to listing
        ArrayList<Image> imageList = new ArrayList<>();
        for (String image : images)
        {
            Image newImage = new Image();
            newImage.setBase64Image(image);
            newImage.setUuid();
            newImage.setListing_id(newListing.getUuid());
            imageList.add(newImage);
        }


        // write the new listing to the database
        try(Connection connection = dataSource.getConnection())
        {
            ModelRepo modelRepo = new ModelRepo(connection);
            //Model model = modelRepo.findByUuid(modelName);
            Model model = modelRepo.findByManufacturerAndModel(brandName, modelName);
            if (model == null)
            {
                session.setAttribute("creationResponse", "Model nicht gefunden");
                response.sendRedirect(request.getContextPath() + "/rent-out");
                return;
            }
            newListing.setModel(model);

            ListingRepo listingRepo = new ListingRepo(connection);
            listingRepo.persist(newListing);
            ImageRepo imageRepo = new ImageRepo(connection);
            for (Image image : imageList)
            {
                imageRepo.persist(image);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            session.setAttribute("creationResponse", "Listing erstellung fehlgeschlagen");
            response.sendRedirect(request.getContextPath() + "/rent-out");
            throw new ServletException(e);
        }
        response.sendRedirect(request.getContextPath() + "/account/listings/?success");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");

        if (user == null)
        {
            response.sendRedirect(request.getContextPath()+ "/login");
            return;
        }
        // get all Brands from Database and send them to the jsp
        try(Connection connection = dataSource.getConnection())
        {
            ModelRepo modelRepo = new ModelRepo(connection);
            List<String> brands = modelRepo.getAllBrands();
            session.setAttribute("brands", brands);
        }
        catch (SQLException e)
        {
            String error = "Fehler beim laden der Marken" + e;
            session.setAttribute("creationResponse", error);
            response.sendRedirect(request.getContextPath() + "/rent-out");
            return;
        }

        session.setAttribute("globalMenuSelection", 3);
        response.sendRedirect(request.getContextPath() + "/rent-out");
    }
}
