/**
 * @Author: Andreas Ott
 */

package de.in.cars4you.servlet.rentout;

import de.in.cars4you.jpa.repo.user.ModelRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet("GetModelsServlet")
public class GetModelsServlet extends HttpServlet {
    
    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();

        String brand = request.getParameter("brand");
        // get all Models of a Brand from the Database and send them to the jsp
        try(Connection connection = dataSource.getConnection())
        {
            ModelRepo modelRepo = new ModelRepo(connection);
            List<String> models = modelRepo.getAllModelsFromBrand(brand);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            // write list into json
            response.getWriter().write("[");
            for(int i = 0; i < models.size(); i++) {
                response.getWriter().write("\"" + models.get(i) + "\"");
                if(i != models.size() - 1) {
                    response.getWriter().write(",");
                }
            }
            response.getWriter().write("]");
        }
        catch (SQLException e)
        {
            throw new ServletException(e);
        }
    }
}
