/**
 * @Author: Johannes Buchberger
 */
package de.in.cars4you.servlet.account;

import de.in.cars4you.jpa.entity.Listing;
import de.in.cars4you.jpa.entity.User;
import de.in.cars4you.jpa.repo.booking.BookingRepo;
import de.in.cars4you.jpa.repo.user.ImageRepo;
import de.in.cars4you.jpa.repo.user.ListingRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet("InserateServlet")
public class InserateServlet extends HttpServlet
{
    @Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        HttpSession session = request.getSession();
        session.setAttribute("accountMenuSelection", 3);
        User user = (User) session.getAttribute("user");

        try (Connection connection = dataSource.getConnection())
        {
            List<Listing> listings = new ListingRepo(connection).findByAuthor(user.getUuid());
            if(listings.size() > 0) session.setAttribute("listings", listings);
            else session.setAttribute("listings", null);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }

        response.sendRedirect(request.getContextPath() + "/account/listings");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        String listingId = request.getParameter("listingId");

        try (Connection connection = dataSource.getConnection())
        {
            new BookingRepo(connection).deleteByListingUuid(listingId);
            ImageRepo imageRepo = new ImageRepo(connection);
            imageRepo.deleteByListingUuid(listingId);
            ListingRepo listingRepo = new ListingRepo(connection);
            listingRepo.delete(listingId);
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }

        response.sendRedirect(request.getContextPath() + "/account/listings");
    }
}
