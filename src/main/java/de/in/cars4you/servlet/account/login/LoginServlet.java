/**
 * @Author: Johannes Buchberger
 */
package de.in.cars4you.servlet.account.login;

import de.in.cars4you.jpa.entity.User;
import de.in.cars4you.jpa.repo.user.UserRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet("LoginServlet")
public class LoginServlet extends HttpServlet
{
    @Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        HttpSession session = request.getSession();
        request.setCharacterEncoding("UTF-8");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        User user = login(email, password);
        if (user != null)
        {
            session.setAttribute("user", user);
            session.setAttribute("loginFail", false);
            response.sendRedirect(request.getContextPath() + "/ProfileServlet");
            return;
        }

        session.setAttribute("loginFail", true);
        response.sendRedirect(request.getContextPath() + "/login");
    }

    private User login(String email, String password)
    {
        try (Connection connection = dataSource.getConnection())
        {
            UserRepo userRepo = new UserRepo(connection);
            User user = userRepo.findByEmail(email);
            connection.close();
            if (user == null)
            {
                return null;
            }
            return password.equals(user.getPassword()) ? user : null;
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
    }
}
