/**
 * Author: Johannes Buchberger
 */
package de.in.cars4you.servlet.account;

import de.in.cars4you.jpa.entity.User;
import de.in.cars4you.jpa.repo.user.UserRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;

@WebServlet("SettingsServlet")
public class SettingsServlet extends HttpServlet
{
    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        HttpSession session = request.getSession();
        session.setAttribute("accountMenuSelection", 4);
        session.setAttribute("updateSuccess", false);
        response.sendRedirect(request.getContextPath() + "/account/settings");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        request.setCharacterEncoding("UTF-8");
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        user.setFirstname(firstname.isBlank() ? user.getFirstname() : firstname);
        user.setLastname(lastname.isBlank() ? user.getLastname() : lastname);
        user.setEmail(email.isBlank() ? user.getEmail() : email);
        user.setPassword(password.isBlank() ? user.getPassword() : password);

        try(Connection connection = dataSource.getConnection())
        {
            UserRepo userRepo = new UserRepo(connection);
            userRepo.update(user);

            session.setAttribute("user", user);
            session.setAttribute("updateSuccess", true);
            response.sendRedirect(request.getContextPath() + "/account/settings");
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
