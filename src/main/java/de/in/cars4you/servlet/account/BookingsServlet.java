/**
 * @Author Andreas Dinauer
 */

package de.in.cars4you.servlet.account;

import de.in.cars4you.dto.BookingDTO;
import de.in.cars4you.jpa.entity.Booking;
import de.in.cars4you.jpa.entity.User;
import de.in.cars4you.jpa.repo.booking.BookingRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("BookingsServlet")
public class BookingsServlet extends HttpServlet
{
    @Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        session.setAttribute("accountMenuSelection", 2);

        try (Connection connection = dataSource.getConnection())
        {
            List<Booking> bookings = new BookingRepo(connection).findByBookedBy(user.getUuid());
            List<Booking> sortedBookings = bookings.stream().sorted(Comparator.comparing(Booking::getBookingDatetime).reversed()).collect(Collectors.toList());
            if(bookings.size() > 0) request.setAttribute("bookings", map(sortedBookings));
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        response.sendRedirect(request.getContextPath() + "/account/bookings");
    }

    private List<BookingDTO> map(List<Booking> bookings) {
        List<BookingDTO> bookingDTOS = new LinkedList<>();
        for (Booking booking : bookings) {
            bookingDTOS.add(new BookingDTO(booking));
        }
        return bookingDTOS;
    }
}
