/**
 * @Author: Johannes Buchberger
 */

package de.in.cars4you.servlet.account;

import de.in.cars4you.jpa.entity.User;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpSession;

@WebServlet("ProfileServlet")
public class ProfileServlet extends jakarta.servlet.http.HttpServlet
{
    public void doGet(jakarta.servlet.http.HttpServletRequest request, jakarta.servlet.http.HttpServletResponse response) throws java.io.IOException, jakarta.servlet.ServletException
    {
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        if (user == null)
        {
            session.setAttribute("globalMenuSelection", 0);
            session.setAttribute("accountMenuSelection", 1);
            response.sendRedirect(request.getContextPath()+ "/login");
            return;
        }
        session.setAttribute("loginFail", false);
        session.setAttribute("registerFail", false);
        session.setAttribute("globalMenuSelection", 0);
        session.setAttribute("accountMenuSelection", 1);
        response.sendRedirect(request.getContextPath() + "/account/profile");
    }
}
