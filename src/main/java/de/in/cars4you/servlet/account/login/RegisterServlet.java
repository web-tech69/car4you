/**
 * @Author Johannes Buchberger
 */

package de.in.cars4you.servlet.account.login;

import de.in.cars4you.jpa.entity.User;
import de.in.cars4you.jpa.repo.user.UserRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

import static java.util.UUID.randomUUID;

@WebServlet("RegisterServlet")
public class RegisterServlet extends HttpServlet
{
    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        request.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String password = request.getParameter("password");
        String email = request.getParameter("email");

        if (firstname.isBlank() || lastname.isBlank() || password.isBlank() || email.isBlank())
        {
            session.setAttribute("registerFail", true);
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }

        User user = createUser(randomUUID().toString(), firstname, lastname, password, email);
        if (user != null)
        {
            session.setAttribute("user", user);
            session.setAttribute("registerFail", false);
            response.sendRedirect(request.getContextPath() + "/ProfileServlet");
            return;
        }

        session.setAttribute("registerFail", true);
        response.sendRedirect(request.getContextPath() + "/login");
    }

    private User createUser(String uuid, String firstname, String lastname, String password, String email)
    {
        try(Connection connection = dataSource.getConnection())
        {
            UserRepo userRepo = new UserRepo(connection);
            User user = new User(uuid, firstname, lastname, password, email, LocalDate.now());
            userRepo.persist(user);
            return user;
        }
        catch (SQLException e)
        {
            return null;
        }
    }
}
