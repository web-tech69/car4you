/**
 * @Author: Andreas Ott, Johannes Buchberger
 */

package de.in.cars4you.servlet.listing;


import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;

@WebServlet("ImageServlet")
public class ImageServlet extends HttpServlet {
    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String uuid = request.getParameter("uuid");
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement ("SELECT * FROM images WHERE uuid = ?");
            pstmt.setString(1, uuid);
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs != null && rs.next()) {
                    Blob bild = rs.getBlob ("image");
                    response.reset();
                    long length = bild.length();
                    response. setHeader("Content-Length",String.valueOf(length));
                    try (InputStream in = bild.getBinaryStream();) {
                        final int bufferSize = 256;
                        byte[] buffer = new byte [bufferSize];
                        ServletOutputStream out = response.getOutputStream ();
                        while ((length = in.read(buffer)) != -1) {
                            out.write(buffer,0, (int) length);
                        }
                        out.flush ();
                    }
                }
            }
        } catch (SQLException ex) {
            throw new ServletException(ex.getMessage());
        }
    }


}
