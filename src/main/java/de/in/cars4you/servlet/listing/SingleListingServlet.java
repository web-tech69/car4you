/**
 * @Author: Andreas Ott
 */
package de.in.cars4you.servlet.listing;

import de.in.cars4you.jpa.entity.Listing;
import de.in.cars4you.jpa.repo.user.ListingRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet("SingleListingServlet")
public class SingleListingServlet extends HttpServlet {

    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try(Connection connection = dataSource.getConnection())
        {
            String currentListingUUID = request.getParameter("currentListing");
            ListingRepo listingRepo = new ListingRepo(connection);
            Listing listing = listingRepo.findByUuid(currentListingUUID);
            request.setAttribute("currentListing", listing);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        response.sendRedirect(request.getContextPath() + "/catalogue/listing");
    }
}
