/**
 * @Author: Andreas Ott
 */
package de.in.cars4you.servlet.listing;

import de.in.cars4you.jpa.entity.Listing;
import de.in.cars4you.jpa.repo.user.ListingRepo;
import de.in.cars4you.jpa.repo.user.ModelRepo;
import de.in.cars4you.utils.SearchRequest;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("ListingServlet")
public class ListingsServlet extends HttpServlet {

    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        try (Connection connection = dataSource.getConnection())
        {
            ListingRepo listingRepo = new ListingRepo(connection);
            Object search = session.getAttribute("search");
            List<Listing> listings;
            if(search instanceof SearchRequest) {
                listings = listingRepo.findBySearch((SearchRequest) search);
            } else {
                listings = listingRepo.getAllListings();
            }
            session.setAttribute("listings", listings);
            session.setAttribute("globalMenuSelection", 2);

            response.sendRedirect(request.getContextPath() + "/catalogue");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        // get all Brands from Database and send them to the jsp
        try(Connection connection = dataSource.getConnection())
        {
            ModelRepo modelRepo = new ModelRepo(connection);
            List<String> brands = modelRepo.getAllBrands();
            brands.add(0, "Alle");
            session.setAttribute("brands", brands);
        }
        catch (SQLException e)
        {
            session.setAttribute("creationResponse", "Fehler beim laden der Marken");
            response.sendRedirect(request.getContextPath() + "/rent-out");
            return;
        }
    }
}
