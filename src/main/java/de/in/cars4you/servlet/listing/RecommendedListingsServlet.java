/**
 * @Author: Andreas Dinauer
 */
package de.in.cars4you.servlet.listing;

import de.in.cars4you.jpa.entity.Listing;
import de.in.cars4you.jpa.repo.user.ListingRepo;
import de.in.cars4you.utils.Recommended;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@WebServlet("RecommendedListingServlet")
public class RecommendedListingsServlet extends HttpServlet {

    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        try (Connection connection = dataSource.getConnection())
        {
            ListingRepo listingRepo = new ListingRepo(connection);
            String[] recommendedBrands = {"Audi", "BMW", "Ford", "Kia"};
            List<Recommended> recommendedListings = new LinkedList<>();
            for(String brand : recommendedBrands) {
                List<Listing> listings = listingRepo.findTop4ByBrand(brand);
                Recommended recommended = new Recommended(brand, listings);
                recommendedListings.add(recommended);
            }
            request.setAttribute("recommended", recommendedListings);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
