// Author: Andreas Ott

package de.in.cars4you.servlet.listing;

import de.in.cars4you.utils.SearchRequest;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import javax.sql.DataSource;
import java.io.IOException;

@WebServlet("SearchServlet")
public class SearchServlet extends HttpServlet {

    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String bookedFrom = request.getParameter("booked-from");
        String bookedTo = request.getParameter("booked-to");
        SearchRequest searchRequest = new SearchRequest(request.getParameter("brand"), request.getParameter("model"), bookedFrom, bookedTo);
        String color = request.getParameter("color");
        String minHorsepower = request.getParameter("horsepower-min");
        String maxHorsepower = request.getParameter("horsepower-max");
        String minManufactureYear = request.getParameter("manufacturing-year-min");
        String maxManufactureYear = request.getParameter("manufacturing-year-max");
        if(color != null) {
            searchRequest.setColor(color);
        }
        if(minHorsepower != null && !minHorsepower.isBlank()) {
            searchRequest.setMinHorsepower(Integer.parseInt(minHorsepower));
        }
        if(maxHorsepower != null && !maxHorsepower.isBlank()) {
            searchRequest.setMaxHorsepower(Integer.parseInt(maxHorsepower));
        }
        if(minManufactureYear != null && !minManufactureYear.isBlank()) {
            searchRequest.setMinManufactureYear(Integer.parseInt(minManufactureYear));
        }
        if(maxManufactureYear != null && !maxManufactureYear.isBlank()) {
            searchRequest.setMaxManufactureYear(Integer.parseInt(maxManufactureYear));
        }
        HttpSession session = request.getSession();
        session.setAttribute("search", searchRequest);
        session.setAttribute("globalMenuSelection", 2);

        response.sendRedirect(request.getContextPath() + "/catalogue");
    }
}