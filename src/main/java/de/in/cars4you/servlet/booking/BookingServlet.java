/**
 * @Author: Andreas Dinauer
 */
package de.in.cars4you.servlet.booking;

import de.in.cars4you.jpa.entity.Booking;
import de.in.cars4you.jpa.entity.User;
import de.in.cars4you.jpa.repo.booking.BookingRepo;
import de.in.cars4you.jpa.repo.user.ListingRepo;
import jakarta.annotation.Resource;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;

@WebServlet("BookingServlet")
public class BookingServlet extends HttpServlet {

    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource dataSource;

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String listingId = request.getParameter("listing-id");
        Object uncastedUser = request.getSession().getAttribute("user");
        try(Connection connection = dataSource.getConnection())  {
            if(uncastedUser instanceof User user) {
                String bookedFrom = request.getParameter("booked-from");
                String bookedTo = request.getParameter("booked-to");
                if(bookedFrom != null && !bookedFrom.isBlank() && bookedTo != null && !bookedTo.isBlank()) {
                    Booking booking = new Booking();
                    booking.setBookedFrom(LocalDate.parse(bookedFrom));
                    booking.setBookedTo(LocalDate.parse(bookedTo));
                    booking.setBookingDatetime(LocalDateTime.now());
                    booking.setBookedBy(user);
                    booking.setListing(new ListingRepo(connection).findByUuid(listingId));
                    new BookingRepo(connection).persist(booking);
                }
                else {
                    response.sendRedirect(request.getContextPath() + "/booking/?currentListing=" + listingId + "&error");
                    return;
                }
            } else {
                response.sendRedirect(request.getContextPath() + "/login");
                return;
            }
        }
        catch(SQLException e) {
            response.sendRedirect(request.getContextPath() + "/booking/?currentListing=" + listingId + "&error");
            e.printStackTrace();
        }
        response.sendRedirect(request.getContextPath() + "/account/bookings/?success");
    }
}
