/**
 * @Author: Johannes Buchberger
 */
package de.in.cars4you.jpa.entity;

import java.time.LocalDate;

public class User
{
    private String uuid;
    private String firstname;
    private String lastname;
    private String password;
    private boolean admin;
    private String email;
    private LocalDate joined_date;

    public User()
    {
        admin = false;
    }

    public User(String uuid, String firstname, String lastname, String password, String email, LocalDate joinedDate)
    {
        this.uuid = uuid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.password = password;
        this.email = email;
        this.admin = false;
        this.joined_date = joinedDate;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public LocalDate getJoinedDate()
    {
        return joined_date;
    }

    public void setJoinedDate(LocalDate joined_date)
    {
        this.joined_date = joined_date;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
