/**
 * @Author: Johannes Buchberger
 */
package de.in.cars4you.jpa.entity;

import java.io.UnsupportedEncodingException;

public class Image
{
    private String uuid;
    private String base64Image;

    private String listing_id;

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }
    public void setUuid() { this.uuid = java.util.UUID.randomUUID().toString(); }

    public byte[] getImageAsBytes() {
        byte[] imageBytes = new byte[0];
        try {
            imageBytes = java.util.Base64.getDecoder().decode(base64Image.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return imageBytes;
    }


    public String getBase64Image()
    {
        return base64Image;
    }
    public void setBase64Image(String base64Image)
    {
        this.base64Image = base64Image;
    }

    public String getListing_id() { return listing_id; }
    public void setListing_id(String listing_id) { this.listing_id = listing_id; }
}
