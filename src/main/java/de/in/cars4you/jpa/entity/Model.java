/**
 * @Author: Johannes Buchberger
 */
package de.in.cars4you.jpa.entity;

public class Model
{
    private String uuid;
    private String manufacturer;
    private String modelName;


    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer)
    {
        this.manufacturer = manufacturer;
    }

    public String getModelName()
    {
        return modelName;
    }

    public void setModelName(String modelName)
    {
        this.modelName = modelName;
    }

}
