/**
 * @Author: Johannes Buchberger
 */
package de.in.cars4you.jpa.entity;

import de.in.cars4you.utils.FuelType;

import java.time.LocalDate;
import java.util.ArrayList;

public class Listing
{
    private String uuid;
    private User author;
    private Model model;

    private String color;
    private ArrayList<String> imageUuid;
    private int manufacturingYear;
    private FuelType fuelType;
    private int fuelCapacity;
    private String doors;
    private int horsepower;
    private int seats;
    private int price;
    private String description;
    private LocalDate createdDate;

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }
    public void setUuid() { this.uuid = java.util.UUID.randomUUID().toString(); }

    public User getAuthor()
    {
        return author;
    }

    public void setAuthor(User author)
    {
        this.author = author;
    }

    public Model getModel()
    {
        return model;
    }

    public void setModel(Model model)
    {
        this.model = model;
    }

    public ArrayList<String> getImageUuid() {
        return imageUuid;
    }

    public void setImage(ArrayList<String> imageUuid) {
        this.imageUuid = imageUuid;
    }

    public int getManufacturingYear()
    {
        return manufacturingYear;
    }

    public void setManufacturingYear(int manufacturingYear)
    {
        this.manufacturingYear = manufacturingYear;
    }

    public int getPrice()
    {
        return price;
    }

    public void setPrice(int price)
    {
        this.price = price;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public LocalDate getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate)
    {
        this.createdDate = createdDate;
    }


    public FuelType getFuelType()
    {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType)
    {
        this.fuelType = fuelType;
    }

    public int getFuelCapacity()
    {
        return fuelCapacity;
    }

    public void setFuelCapacity(int fuelCapacity)
    {
        this.fuelCapacity = fuelCapacity;
    }

    public String getDoors()
    {
        return doors;
    }

    public void setDoors(String doors)
    {
        this.doors = doors;
    }

    public int getHorsepower()
    {
        return horsepower;
    }

    public void setHorsepower(int horsepower)
    {
        this.horsepower = horsepower;
    }

    public int getSeats()
    {
        return seats;
    }

    public void setSeats(int seats)
    {
        this.seats = seats;
    }
}
