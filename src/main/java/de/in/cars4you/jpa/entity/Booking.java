/**
 * @Author: Johannes Buchberger
 */
package de.in.cars4you.jpa.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Booking
{
    private String uuid;
    private Listing listing;
    private User bookedBy;
    private LocalDateTime bookingDatetime;
    private LocalDate bookedFrom;
    private LocalDate bookedTo;

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    public Listing getListing()
    {
        return listing;
    }

    public void setListing(Listing listing)
    {
        this.listing = listing;
    }

    public User getBookedBy()
    {
        return bookedBy;
    }

    public void setBookedBy(User bookedBy)
    {
        this.bookedBy = bookedBy;
    }

    public LocalDateTime getBookingDatetime()
    {
        return bookingDatetime;
    }

    public void setBookingDatetime(LocalDateTime bookingDatetime)
    {
        this.bookingDatetime = bookingDatetime;
    }

    public LocalDate getBookedFrom()
    {
        return bookedFrom;
    }

    public void setBookedFrom(LocalDate bookedFrom)
    {
        this.bookedFrom = bookedFrom;
    }

    public LocalDate getBookedTo()
    {
        return bookedTo;
    }

    public void setBookedTo(LocalDate bookedTo)
    {
        this.bookedTo = bookedTo;
    }
}
