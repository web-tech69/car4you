/**
 * @Author: Andreas Ott
 */
package de.in.cars4you.jpa.repo.user;

import de.in.cars4you.jpa.entity.*;
import de.in.cars4you.jpa.repo.booking.BookingRepo;
import de.in.cars4you.utils.FuelType;
import de.in.cars4you.utils.SearchRequest;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.System.currentTimeMillis;

public class ListingRepo {
    private final Connection connection;

    public ListingRepo(Connection dSconnection) {
        this.connection = dSconnection;
    }

    // find by UUID
    public Listing findByUuid(String uuid) {
        String sql = "SELECT * FROM listings WHERE uuid = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, uuid);
            ResultSet resultSet = statement.executeQuery();
            return getListingFromResultset(resultSet).get(0);
        } catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
            // Change to Listing Null and Log message?
            return null;
        }
    }

    // Function to get All Listing from DB
    public ArrayList<Listing> getAllListings() {
        String sql = "SELECT * FROM listings";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            return getListingFromResultset(resultSet);
        } catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
            return null;
        }
    }

    public List<Listing> findTop4ByBrand(String brand) {
        String sql = "SELECT * FROM listings AS l, models AS m WHERE l.model_id = m.uuid AND m.manufacturer = ? LIMIT 4";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, brand);
            ResultSet resultSet = statement.executeQuery();
            return getListingFromResultset(resultSet);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Listing> findBySearch(SearchRequest searchRequest) {
        if(searchRequest.getColor() == null) {
            return standardSearch(searchRequest);
        }
        else if(searchRequest.getColor() != null) {
            return colorSearch(searchRequest);
        }
        return null;
    }

    private boolean checkBookings(Listing listing, LocalDate bookedFrom, LocalDate bookedTo) {
        List<Booking> bookings = new BookingRepo(connection).findByListingId(listing.getUuid());
        for(Booking booking : bookings) {
            // Bereits bestehende Buchungen nach Überschneidung mit gewünschtem Buchungszeitraum prüfen
            if(bookedFrom != null && bookedTo != null && bookedFrom.isBefore(booking.getBookedTo()) && bookedTo.isAfter(booking.getBookedFrom())) {
                return false;
            }
        }
        return true;
    }

    public void deleteByUuid(String uuid) {
        String sql = "DELETE FROM listings WHERE uuid = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, uuid);
            statement.executeUpdate();
        } catch (java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteByUserUuid(String uuid) {
        String sql = "DELETE FROM listings WHERE author_id = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, uuid);
            statement.executeUpdate();
        } catch (java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ArrayList<Listing> getListingFromResultset(ResultSet rs) {
        ArrayList<Listing> listings = new ArrayList<>();
        try {
            while (rs.next())
            {
                Listing listing = new Listing();
                listing.setUuid(rs.getString("uuid"));
                // Get Author from database
                UserRepo userRepo = new UserRepo(connection);
                User user = userRepo.findByUuid(rs.getString("author_id"));
                listing.setAuthor(user);
                // Get Model from database
                ModelRepo modelRepo = new ModelRepo(connection);
                Model model = modelRepo.findByUuid(rs.getString("model_id"));
                listing.setModel(model);
                // Get Image from database
                ImageRepo imageRepo = new ImageRepo(connection);
                List<String> images = imageRepo.findByListingUuid(rs.getString("uuid")).stream().map(Image::getUuid).toList();
                listing.setImage(new ArrayList<>(images));
                listing.setPrice(rs.getInt("price"));
                listing.setDescription(rs.getString("description"));
                listing.setColor(rs.getString("color"));
                listing.setCreatedDate(rs.getDate("created_date").toLocalDate());
                listing.setManufacturingYear(rs.getInt("manufacturing_year"));
                listing.setFuelType(FuelType.valueOf(rs.getString("fuel_type")));
                listing.setFuelCapacity(rs.getInt("fuel_capacity"));
                listing.setDoors(rs.getString("doors"));
                listing.setHorsepower(rs.getInt("horsepower"));
                listing.setSeats(rs.getInt("seats"));
                listings.add(listing);
            }
        }
        catch (SQLException e)
        {
            throw new RuntimeException(e);
        }
        return listings;
    }

    public void persist(Listing listing) throws SQLException
    {
        String sql = "INSERT INTO listings (uuid, author_id, model_id, price, color, description, created_date, manufacturing_year, fuel_type, fuel_capacity, doors, horsepower, seats) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, listing.getUuid());
        statement.setString(2, listing.getAuthor().getUuid());
        statement.setString(3, listing.getModel().getUuid());
        statement.setInt(4, listing.getPrice());
        statement.setString(5, listing.getColor());
        statement.setString(6, listing.getDescription());
        statement.setDate(7, new Date(currentTimeMillis()));
        statement.setInt(8, listing.getManufacturingYear());
        statement.setString(9, listing.getFuelType().toString());
        statement.setInt(10, listing.getFuelCapacity());
        statement.setString(11, listing.getDoors());
        statement.setInt(12, listing.getHorsepower());
        statement.setInt(13, listing.getSeats());
        statement.executeUpdate();
    }

    public List<Listing> findByAuthor(String uuid) throws SQLException
    {
        String sql = "SELECT * FROM listings WHERE author_id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, uuid);
        ResultSet resultSet = statement.executeQuery();
        return getListingFromResultset(resultSet);
    }

    public void delete(String listingId) throws SQLException
    {
        String sql = "DELETE FROM listings WHERE uuid = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, listingId);
        statement.executeUpdate();
    }




    public List<Listing> standardSearch(SearchRequest searchRequest) {
        String sql = "SELECT * FROM listings AS l, models AS m WHERE l.model_id = m.uuid AND m.manufacturer LIKE ? AND m.model_name LIKE ? AND l.horsepower > ? AND l.horsepower < ? AND l.manufacturing_year > ? AND l.manufacturing_year < ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, searchRequest.getBrand());
            statement.setString(2, searchRequest.getModel());
            statement.setInt(3, searchRequest.getMinHorsepower());
            statement.setInt(4, searchRequest.getMaxHorsepower());
            statement.setInt(5, searchRequest.getMinManufactureYear());
            statement.setInt(6, searchRequest.getMaxManufactureYear());

            ResultSet resultSet = statement.executeQuery();
            List<Listing> listings = getListingFromResultset(resultSet);
            return listings.stream()
                    //Filtern von gebuchten Fahrzeugen
                    .filter(listing -> checkBookings(listing, searchRequest.getBookedFrom(), searchRequest.getBookedTo()))
                    .collect(Collectors.toList());
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Listing> colorSearch(SearchRequest searchRequest) {
        String sql = "SELECT * FROM listings AS l, models AS m WHERE l.model_id = m.uuid AND m.manufacturer LIKE ? AND m.model_name LIKE ? AND l.color LIKE ? AND l.horsepower > ? AND l.horsepower < ? AND l.manufacturing_year > ? AND l.manufacturing_year < ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, searchRequest.getBrand());
            statement.setString(2, searchRequest.getModel());
            statement.setString(3, searchRequest.getColor());
            statement.setInt(4, searchRequest.getMinHorsepower());
            statement.setInt(5, searchRequest.getMaxHorsepower());
            statement.setInt(6, searchRequest.getMinManufactureYear());
            statement.setInt(7, searchRequest.getMaxManufactureYear());
            ResultSet resultSet = statement.executeQuery();
            List<Listing> listings = getListingFromResultset(resultSet);
            return listings.stream()
                    //Filtern von gebuchten Fahrzeugen
                    .filter(listing -> checkBookings(listing, searchRequest.getBookedFrom(), searchRequest.getBookedTo()))
                    .collect(Collectors.toList());
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}