/**
 * @Author Johannes Buchberger
 */
package de.in.cars4you.jpa.repo.user;

import de.in.cars4you.jpa.entity.User;

import java.sql.*;
import java.util.ArrayList;

import static java.lang.System.currentTimeMillis;
import static java.util.UUID.randomUUID;

public class UserRepo
{
    private Connection DSconnection;

    public UserRepo(Connection DSconnectio)
    {

        this.DSconnection = DSconnectio;
    }

    public ArrayList<User> findAll()
    {
        String sql = "SELECT * FROM users";
        try (PreparedStatement statement = DSconnection.prepareStatement(sql))
        {
            ResultSet resultSet = statement.executeQuery();
            ArrayList<User> users = new ArrayList<>();
            while (resultSet.next())
            {
                users.add(getUserFromResultset(resultSet));
            }
            return users;
        }
        catch (java.sql.SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public User findByEmail(String email)
    {
        String sql = "SELECT * FROM users WHERE email = ?";
        try (PreparedStatement statement = DSconnection.prepareStatement(sql))
        {
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return getUserFromResultset(resultSet);
        }
        catch (java.sql.SQLException e)
        {
            //throw new RuntimeException(e);
            // Change to User Null and Log message??
            // Otherwise error on Login with wrong Username
            return null;
        }
    }

    public User findByUuid(String uuid)
    {
        String sql = "SELECT * FROM users WHERE uuid = ?";
        try (PreparedStatement statement = DSconnection.prepareStatement(sql))
        {
            statement.setString(1, uuid);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return getUserFromResultset(resultSet);
        }
        catch (java.sql.SQLException e)
        {
            //throw new RuntimeException(e);
            // Change to User Null and Log message??
            // Otherwise error on Login with wrong Username
            return null;
        }
    }

    public void deleteByUuid(String uuid)
    {
        String sql = "DELETE FROM users WHERE uuid = ?";
        try (PreparedStatement statement = DSconnection.prepareStatement(sql))
        {
            statement.setString(1, uuid);
            statement.executeUpdate();
        }
        catch (java.sql.SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public User getUserFromResultset(ResultSet resultSet) throws SQLException
    {
        User user = new User();
        user.setUuid(resultSet.getString("uuid"));
        user.setFirstname(resultSet.getString("firstname"));
        user.setLastname(resultSet.getString("lastname"));
        user.setPassword(resultSet.getString("password"));
        user.setEmail(resultSet.getString("email"));
        try
        {
            user.setAdmin(resultSet.getBoolean("admin"));
        }
        catch (Exception e)
        {
            user.setAdmin(false);
        }

        user.setJoinedDate(resultSet.getDate("joined_date").toLocalDate());
        return user;
    }

    public void persist(User user) throws SQLException
    {
        String sql = "INSERT INTO users (uuid, firstname, lastname, password, email, admin, joined_date) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = DSconnection.prepareStatement(sql);
        String uuid = randomUUID().toString();
        statement.setString(1, uuid);
        statement.setString(2, user.getFirstname());
        statement.setString(3, user.getLastname());
        statement.setString(4, user.getPassword());
        statement.setString(5, user.getEmail());
        statement.setBoolean(6, user.isAdmin());
        statement.setDate(7, new Date(currentTimeMillis()));
        statement.executeUpdate();
        user.setUuid(uuid);
    }

    public void update(User user) throws SQLException
    {
        String sql = "UPDATE users SET firstname = ?, lastname = ?, password = ?, email = ? WHERE uuid = ?";
        PreparedStatement statement = DSconnection.prepareStatement(sql);
        statement.setString(1, user.getFirstname());
        statement.setString(2, user.getLastname());
        statement.setString(3, user.getPassword());
        statement.setString(4, user.getEmail());
        statement.setString(5, user.getUuid());
        statement.executeUpdate();
    }
}
