/**
 * Andreas Konrad Ott, Johannes Buchberger, Andreas Johannes Dinauer
 */
package de.in.cars4you.jpa.repo.booking;

import de.in.cars4you.jpa.entity.Booking;
import de.in.cars4you.jpa.entity.User;
import de.in.cars4you.jpa.repo.user.ListingRepo;
import de.in.cars4you.jpa.repo.user.UserRepo;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import static java.lang.System.currentTimeMillis;
import static java.util.UUID.randomUUID;

public class BookingRepo {

    private Connection connection;

    public BookingRepo(Connection connection) {
        this.connection = connection;
    }

    public List<Booking> findByBookedBy(String userId) {
        String sql = "SELECT * FROM bookings WHERE booked_by = ?";
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            ResultSet resultSet = statement.executeQuery();
            return getBookingsFromResultset(resultSet);
        }
        catch (java.sql.SQLException e) {
            return null;
        }
    }

    public List<Booking> findByListingId(String listingId) {
        String sql = "SELECT * FROM bookings WHERE listing_id = ?";
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, listingId);
            ResultSet resultSet = statement.executeQuery();
            return getBookingsFromResultset(resultSet);
        }
        catch (java.sql.SQLException e) {
            return null;
        }
    }

    public void deleteByUserUuid(String userUuid) {
        String sql = "DELETE FROM bookings WHERE booked_by = ?";
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userUuid);
            statement.executeUpdate();
        }
        catch (java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }



    public void deleteByListingUuid(String listingId) {
        String sql = "DELETE FROM bookings WHERE listing_id = ?";
        try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, listingId);
            statement.executeUpdate();
        }
        catch (java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private List<Booking> getBookingsFromResultset(ResultSet rs) throws SQLException {
        List<Booking> result = new LinkedList<>();
        while(rs.next()) {
            ListingRepo listingRepo = new ListingRepo(connection);

            Booking booking = new Booking();
            UserRepo userRepo = new UserRepo(connection);
            booking.setBookedBy(userRepo.findByUuid(rs.getString("booked_by")));
            booking.setUuid(rs.getString("uuid"));
            booking.setListing(listingRepo.findByUuid(rs.getString("listing_id")));
            booking.setBookedFrom(rs.getDate("booked_from").toLocalDate());
            booking.setBookedTo(rs.getDate("booked_to").toLocalDate());
            booking.setBookingDatetime(rs.getTimestamp("booking_datetime").toLocalDateTime());

            result.add(booking);
        }
        return result;
    }

    public void persist(Booking booking) throws SQLException
    {
        String sql = "INSERT INTO bookings (uuid, listing_id, booked_by, booking_datetime, booked_from, booked_to) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        String uuid = randomUUID().toString();
        statement.setString(1, uuid);
        statement.setString(2, booking.getListing().getUuid());
        statement.setString(3, booking.getBookedBy().getUuid());
        statement.setTimestamp(4, Timestamp.valueOf(booking.getBookingDatetime()));
        statement.setDate(5, Date.valueOf(booking.getBookedFrom()));
        statement.setDate(6, Date.valueOf(booking.getBookedTo()));
        statement.executeUpdate();
    }

}
