/**
 * @Author: Andreas Ott
 */
package de.in.cars4you.jpa.repo.user;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;

import de.in.cars4you.jpa.entity.Image;
public class ImageRepo {
    private Connection DSconnection;

    public ImageRepo(Connection DSconnection) {
        this.DSconnection = DSconnection;
    }

    public Image findByUuid(String uuid) {
        String sql = "SELECT * FROM images WHERE uuid = ?";
        try (PreparedStatement statement = DSconnection.prepareStatement(sql)) {
            statement.setString(1, uuid);
            ResultSet resultSet = statement.executeQuery();
            Image image;
            resultSet.next();
            image = getImageFromResultset(resultSet);
            return image;
        } catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
            return null;
        }
    }

    public ArrayList<Image> findByListingUuid(String listingUuid) {
        String sql = "SELECT * FROM images WHERE listing_id = ?";
        try (PreparedStatement statement = DSconnection.prepareStatement(sql)) {
            statement.setString(1, listingUuid);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Image> images = new ArrayList<>();
            while (resultSet.next()) {
                images.add(getImageFromResultset(resultSet));
            }
            return images;
        } catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
            return null;
        }
    }
    public void deleteByListingUuid(String listingUuid) {
        String sql = "DELETE FROM images WHERE listing_id = ?";
        try (PreparedStatement statement = DSconnection.prepareStatement(sql)) {
            statement.setString(1, listingUuid);
            statement.executeUpdate();
        } catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
        }
    }

    public Image getImageFromResultset(ResultSet resultSet) throws SQLException {
        Image image = new Image();
        byte[] imageBytes;
        imageBytes = resultSet.getBytes("image");
        image.setUuid(resultSet.getString("uuid"));
        String base64Image = Base64.getEncoder().encodeToString(imageBytes);
        image.setBase64Image(base64Image);
        return image;
    }

    public void persist(Image image) {
        String sql = "INSERT INTO images (uuid, image, listing_id) VALUES (?, ?, ?)";
        try (PreparedStatement statement = DSconnection.prepareStatement(sql)) {
            statement.setString(1, image.getUuid());
            statement.setBytes(2, image.getImageAsBytes());
            statement.setString(3, image.getListing_id());
            statement.executeUpdate();
        } catch (java.sql.SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
