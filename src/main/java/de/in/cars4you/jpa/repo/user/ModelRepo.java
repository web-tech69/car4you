/**
 * @Author: Andreas Ott
 */
package de.in.cars4you.jpa.repo.user;

import de.in.cars4you.jpa.entity.Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ModelRepo {
    private final Connection dSconnection;

    public ModelRepo(Connection DSconnection) {
        this.dSconnection = DSconnection;
    }

    // find by Manufacturer
    public ArrayList<Model> findByManufacturer(String manufacturer) {
        String sql = "SELECT * FROM models WHERE manufacturer = ?";
        try(PreparedStatement statement = dSconnection.prepareStatement(sql)) {
            statement.setString(1, manufacturer);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Model> models = new ArrayList<>();
            while(resultSet.next()) {
                models.add(getModelFromResultset(resultSet));
            }
            return models;
        }
        catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
            // Change to Model Null and Log message?
            return null;
        }
    }
    public Model findByManufacturerAndModel(String manufacturer, String modelName) {
        String sql = "SELECT * FROM models WHERE manufacturer = ? AND model_name = ?";
        try(PreparedStatement statement = dSconnection.prepareStatement(sql)) {
            statement.setString(1, manufacturer.trim());
            statement.setString(2, modelName.trim());
            ResultSet resultSet = statement.executeQuery();
            Model model = null;
            if(resultSet.next()) {
                model = getModelFromResultset(resultSet);
            }
            return model;
        }
        catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
            // Change to Model Null and Log message?
            return null;
        }
    }

    // find by UUID
    public Model findByUuid(String uuid) {
        String sql = "SELECT * FROM models WHERE uuid = ?";
        try(PreparedStatement statement = dSconnection.prepareStatement(sql)) {
            statement.setString(1, uuid);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Model model1 = getModelFromResultset(resultSet);
            return model1;
        }
        catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
            // Change to Model Null and Log message?
            return null;
        }
    }

    // Function to get All Models from Brand from DB
    public ArrayList<String> getAllModelsFromBrand(String brand) {
        String sql = "SELECT model_name FROM models WHERE manufacturer = ? ORDER BY model_name ASC";
        try(PreparedStatement statement = dSconnection.prepareStatement(sql)) {
            statement.setString(1, brand);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<String> modelNames = new ArrayList<>();
            while(resultSet.next()) {
                modelNames.add(resultSet.getString("model_name"));
            }
            return modelNames;
        }
        catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
            return null;
        }
    }

    public ArrayList<String> getAllBrands() {
        String sql = "SELECT DISTINCT manufacturer FROM models ORDER BY manufacturer ASC";
        try(PreparedStatement statement = dSconnection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            ArrayList<String> brandNames = new ArrayList<>();
            while(resultSet.next()) {
                brandNames.add(resultSet.getString("manufacturer"));
            }
            return brandNames;
        }
        catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
            return null;
        }
    }

    public ArrayList<Model> findByModelName(String modelName) {
        String sql = "SELECT * FROM models WHERE model_name = ?";
        try(PreparedStatement statement = dSconnection.prepareStatement(sql)) {
            statement.setString(1, modelName);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Model> models = new ArrayList<>();
            while(resultSet.next()) {
                Model model = getModelFromResultset(resultSet);
                models.add(model);
            }
            return models;
        }
        catch (java.sql.SQLException e) {
            //throw new RuntimeException(e);
            // Change to Model Null and Log message?
            return null;
        }
    }
    public void persist(Model model) throws SQLException {
        String sql = "INSERT INTO models (uuid, manufacturer, model_name) VALUES (? ,? , ?)";
        PreparedStatement statement = dSconnection.prepareStatement(sql);
        statement.setString(1, model.getUuid());
        statement.setString(2, model.getManufacturer());
        statement.setString(3, model.getModelName());
        statement.executeUpdate();
        ResultSet resultSet = statement.getGeneratedKeys();
        resultSet.next();
        model.setUuid(resultSet.getString(1));
    }
    private Model getModelFromResultset(ResultSet resultSet) throws SQLException {
        Model model = new Model();
        model.setUuid(resultSet.getString("uuid"));
        model.setManufacturer(resultSet.getString("manufacturer"));
        model.setModelName(resultSet.getString("model_name"));
        return model;
    }
}


