//Author: Andreas Dinauer

package de.in.cars4you.utils;

import de.in.cars4you.jpa.entity.Listing;

import java.util.List;

public class Recommended {

    private String brandName;
    private List<Listing> listings;

    public Recommended(String brandName, List<Listing> listings) {
        this.brandName = brandName;
        this.listings = listings;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public List<Listing> getListings() {
        return listings;
    }

    public void setListings(List<Listing> listings) {
        this.listings = listings;
    }
}
