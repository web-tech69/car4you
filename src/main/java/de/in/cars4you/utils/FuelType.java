//Author: Andreas Ott

package de.in.cars4you.utils;

public enum FuelType {
    DIESEL, GASOLINE, ELECTRIC, HYBRID, HYDROGEN, OTHER
}
