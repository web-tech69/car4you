//Author: Andreas Dinauer

package de.in.cars4you.utils;

import java.time.LocalDate;

public class SearchRequest {

    private String brand;
    private String model;
    private LocalDate bookedFrom;
    private LocalDate bookedTo;
    private String color;
    private int minHorsepower;
    private int maxHorsepower;
    private int minManufactureYear;
    private int maxManufactureYear;

    public SearchRequest(String brand, String model, String bookedFrom, String bookedTo) {
        this.brand = set(brand);
        this.model = set(model);
        this.bookedFrom = setDate(bookedFrom);
        this.bookedTo = setDate(bookedTo);
        this.minHorsepower = 0;
        this.maxHorsepower = 9999;
        this.minManufactureYear = 0;
        this.maxManufactureYear = 9999;
    }

    private String set(String value) {
        if(value != null && !value.isBlank() && !value.equals("Alle")) {
            return value;
        }
        return "%";
    }

    private LocalDate setDate(String date) {
        if(date != null && !date.isBlank()) {
            return LocalDate.parse(date);
        }
        return null;
    }

    public String getBrand() { return brand; }
    public void setBrand(String brand) { this.brand = brand; }
    public String getModel() { return model; }
    public LocalDate getBookedFrom() { return bookedFrom; }
    public LocalDate getBookedTo() { return bookedTo; }
    public String getColor() { return color; }
    public void setColor(String color) { this.color = color; }
    public void setMinHorsepower(int minHorsepower) { this.minHorsepower = minHorsepower; }
    public int getMinHorsepower() { return minHorsepower; }
    public void setMaxHorsepower(int maxHorsepower) { this.maxHorsepower = maxHorsepower; }
    public int getMaxHorsepower() { return maxHorsepower; }
    public void setMinManufactureYear(int minManufactureYear) { this.minManufactureYear = minManufactureYear; }
    public int getMinManufactureYear() { return minManufactureYear; }
    public void setMaxManufactureYear(int maxManufactureYear) { this.maxManufactureYear = maxManufactureYear; }
    public int getMaxManufactureYear() { return maxManufactureYear; }
}
